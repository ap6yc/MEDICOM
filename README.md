# MEDICOM

This project encapsulates all code and documentation of the MEDICOM project for the AREUS laboratory of the Missouri University of Science and Technology.

| Master | Develop |
| --- | --- |
| [![pipeline status](https://gitlab.com/ap6yc/MEDICOM/badges/master/pipeline.svg)](https://gitlab.com/ap6yc/MEDICOM/commits/master) | [![pipeline status](https://gitlab.com/ap6yc/MEDICOM/badges/develop/pipeline.svg)](https://gitlab.com/ap6yc/MEDICOM/commits/develop) |

# Installation

###### Requirements:
* ```conda``` (package manager)
* ```numpy``` (numerical computing)
* ```scipy``` (scientific computing)
* ```scikit-image``` (skimage)
* ```matplotlib``` (plotting in python)

* ```graphviz``` (graph visualization)
* ```pydot``` (I have no clue)
* ```h5py``` (saving keras models)
* ```tensorflow``` (base ANN library, contains an implementation of ```keras```)

###### Optional Requirements
* ```sphinx``` (compiles documentation locally)
* ```sphinxcontrib-matlabdomain``` (MATLAB syntax in sphinx documentation)
* sphinx_rtd_theme (theme used for documentation)

# Usage

The development on this project is in a perpetual state of flux. Therefore, it is borderline unethical to write any word on the software's use. Instead, the reader is asked to consult the [Sasha](sap625@mst.edu).

# Structure

The following file-tree summarizes the project structure:

```
MEDICOM
├── assets                  // Project-wide assets
├── dockerfiles             // Docker files for the project
├── docs                    // Sphinx documentation
├── documentation           // Documentation (pdfs, etc.)
├── GMAT                    // GMAT scripts for the project
├── LaTeX                   // All LaTeX efforts for the project
├── MATLAB                  // MATLAB code of the project
├── papers                  // Relevant literature storage
├── public                  // Release location (empty) for Sphinx files
├── pyMEDICOM               // Python module for the project
├── SPICE                   // Location for SPICE files/kernels
└── README.md
```

# Contributing

1. Ask permission from the [Sasha](sap625@mst.edu).
2. GOTO 1.

# History

* 8/29/2018 - Begin project
* 9/19/2018 - Successful keras and GMAT implementations, pre-integration
* 11/21/2018 - Migrate development to MATLAB-driven data generation

# Credits

#### Authors

* Author: Sasha Petrenko <sap625@mst.edu>
* Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

#### Software:

###### MATLAB:

* Struct-struct: https://www.mathworks.com/matlabcentral/fileexchange/32879-graphically-display-the-branch-structure-of-a-struct-variable
* YamlMATLAB: https://github.com/ewiger/yamlmatlab or https://code.google.com/archive/p/yamlmatlab/

###### Python:

* Tensorflow's implementation of ```keras```: https://www.tensorflow.org/guide/keras

# License

This software is owned by the AREUS laboratory of the Missouri University of Science and Technology.
