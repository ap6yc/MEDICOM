# -*- coding: utf-8 -*-
"""
Document: naive_detection.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Performs naive detection of performed maneuvers on angles-only information

@author: sap625
"""

#=============================================================================
# IMPORTS
#=============================================================================

# External imports
from __future__ import print_function

import os
import sys
import copy
from time import time
import pickle
from pathlib import Path

import numpy as np

import tensorflow as tf

from tensorflow.python import keras
from tensorflow.python.keras.datasets import mnist
from tensorflow.python.keras.layers import Dense, Flatten
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.models import load_model

import matplotlib.pylab as plt

import scipy.io as sio

#import logging
#from matplotlib import pyplot as plt
#import os

# Local imports


#=============================================================================
# DEFS
#=============================================================================

class AccuracyHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.acc = []

    def on_epoch_end(self, batch, logs={}):
        self.acc.append(logs.get('acc'))

def save_object(obj, filename):
    """
    Saves a python object via pickle

    :param obj: object to save
    :param filename: filename
    :type obj: Object
    :type filename: str
    :return: None
    :rtype: None
    """
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def load_object(filename):
    """
    Loads a Python object via pickle

    :param filename: filename
    :type filename: str
    :return: obj
    :rtype: Object
    """

    with open(filename, 'rb') as to_load:
        obj = pickle.load(to_load)
        return obj

#=============================================================================
# MAIN
#=============================================================================

def main():
    """Main function

    INPUTS: N/A
    OUTPUTS: N/A
    """

    # Setup environment
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Just disables the warning, doesn't enable AVX/FMA

    batch_size = 128
    num_classes = 10
    epochs = 10

    # input image dimensions
    img_x, img_y = 28, 28

    # load the MNIST data set, which already splits into train and test sets for us
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    # reshape the data into a 4D tensor - (sample_number, x_img_size, y_img_size, num_channels)
    # because the MNIST is greyscale, we only have a single channel - RGB colour images would have 3
    x_train = x_train.reshape(x_train.shape[0], img_x, img_y, 1)
    x_test = x_test.reshape(x_test.shape[0], img_x, img_y, 1)
    input_shape = (img_x, img_y, 1)

    # convert the data to the right type
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    # convert class vectors to binary class matrices - this is for use in the
    # categorical_crossentropy loss below
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    model_file = 'model.h5'
    model_path = Path(model_file)
    history_acc_file = 'history_acc.pkl'

    if not model_path.is_file():

        model = Sequential()
        model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                         activation='relu',
                         input_shape=input_shape))
        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.Adam(),
                      metrics=['accuracy'])

        history = AccuracyHistory()

        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  epochs=epochs,
                  verbose=1,
                  validation_data=(x_test, y_test),
                  callbacks=[history])

        model.save(model_file)
        save_object(history.acc, history_acc_file)
        history_acc = history.acc
    else:
        model = load_model(model_file)
        history_acc = load_object(history_acc_file)

    # Evaluate and plot
    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    plt.plot(range(1, 11), history_acc)
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.show()

    return

#=============================================================================
# RUN MAIN
#=============================================================================

if __name__ == '__main__':
    main()
    print('did stuff')
