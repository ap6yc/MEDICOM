import numpy as np
from hmmlearn import hmm
np.random.seed(42)

# model = hmm.GaussianHMM(n_components=3, covariance_type="full")
# model.startprob_ = np.array([0.6, 0.3, 0.1])
# model.transmat_ = np.array([[0.7, 0.2, 0.1],
#                              [0.3, 0.5, 0.2],
#                              [0.3, 0.3, 0.4]])
# model.means_ = np.array([[0.0, 0.0], [3.0, -3.0], [5.0, 10.0]])
# model.covars_ = np.tile(np.identity(2), (3, 1, 1))
# X, Z = model.sample(100)

# b = [[np.random.rand] for x in ]
a = np.random.rand(1000,1)*4 + 4
b = np.random.rand(1000,1)*4

test = [[5],
        [.4],
        [2],
        [5],
        [.1],
        [.3],
        [3],
        [7]]

# Pack diff and volume for training.
# X = np.column_stack([x, t])

# Make an HMM instance and execute fit
# model = hmm.GaussianHMM(n_components=2, covariance_type="diag", n_iter=1000)
# model.fit(X)
# Predict the optimal sequence of internal hidden state
# hidden_states = model.predict(test)

ma = hmm.GaussianHMM(n_components=2, covariance_type="diag", n_iter=10000).fit(a)
mb = hmm.GaussianHMM(n_components=2, covariance_type="diag", n_iter=10000).fit(b)

ha = ma.predict(test)
hb = mb.predict(test)

print(ha)
print(hb)