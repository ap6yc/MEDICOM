# -*- coding: utf-8 -*-
"""
Document: mc_pi.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Monte Carlo method to determine pi, demonstrating random process for determining (potentially) deterministic values/distributions

@author: sap625
"""

# External imports
from __future__ import print_function

import os
import sys
import numpy as np
import matplotlib.pylab as plt

plt.close('all')

def get_range(x, y):
    return np.sqrt(x**2 + y**2)

data = []

for i in range(1000):
    x = np.random.rand()
    y = np.random.rand()
    r = get_range(x, y)
    if r < 1:
        c = 1
    else:
        c = 0
    data.append([x, y, c])

# print(data)
length = len(data)

np_data = np.array(data)

is_in = np.sum(np_data[::,2])
is_out = length - is_in

mc_pi = is_in/length

print(mc_pi*4)
colors = []
for i in range(length):
    if np_data[i, 2] == 0:
        colors.append('b')
    else:
        colors.append('r')

plt.scatter(np_data[::, 0], np_data[::, 1], c=colors)
plt.show()
