# -*- coding: utf-8 -*-
"""
Document: test_normalize.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Test for the normalization of the classification data

@author: sap625
"""

#=============================================================================
# IMPORTS
#=============================================================================

# External imports
# from __future__ import print_function

import os
import numpy as np
import pickle
from pathlib import Path
# sys.path.append(os.path.join(os.path.dirname(__file__), data_dir))

import scipy.io as sio

#=============================================================================
# DEFS
#=============================================================================

def save_object(obj, filename):
    """
    Saves a python object via pickle

    :param obj: object to save
    :param filename: filename
    :type obj: Object
    :type filename: str
    :return: None
    :rtype: None
    """
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def load_object(filename):
    """
    Loads a Python object via pickle

    :param filename: filename
    :type filename: str
    :return: obj
    :rtype: Object
    """

    with open(filename, 'rb') as to_load:
        obj = pickle.load(to_load)
        return obj

#=============================================================================
# MAIN
#=============================================================================

"""Main function

INPUTS: N/A
OUTPUTS: N/A
"""

# Setup environment
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Just disables the warning, doesn't enable AVX/FMA

# MATLAB load operations
data_dir = '../../data'
data_file = 'data_detection.mat'
data_full = os.path.join(data_dir, data_file)

# Model
model_file = 'model.h5'
model_path = Path(data_dir).joinpath(model_file)
history_acc_file = 'history_acc.pkl'

# Load the data
try:
#    'Tm_c', 'Zt_c', 'Zm_c','class'
    sio.whosmat(data_full)
    data = sio.loadmat(data_full)
except:
    print('MATLAB data is missing')
# data_path = Path(data_full)
# if not data_path.is_file():

# batch_size = 128
batch_size = 32
num_classes = 2
epochs = 10

#    print(data['train'])
#    x_data = np.array(data['train'])
x_data = np.array(data['train'])
x_data = x_data / np.pi
print('Maximum: ', np.max(x_data))
print('Minimum: ', np.min(x_data))
# x_data = normalize(x_data, order = 2)
#print(x_data)