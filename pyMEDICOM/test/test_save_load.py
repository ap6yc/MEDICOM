# -*- coding: utf-8 -*-
"""
Document: naive_detection.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Performs naive detection of performed maneuvers on angles-only information

@author: sap625
"""

#=============================================================================
# IMPORTS
#=============================================================================

# External imports
# from __future__ import print_function

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

import copy
from time import time
import pickle
from pathlib import Path
# sys.path.append(os.path.join(os.path.dirname(__file__), data_dir))

import numpy as np

import tensorflow as tf

from tensorflow.python import keras
#from tensorflow.python.keras.datasets import mnist
from tensorflow.python.keras.layers import Dense, Flatten, Dropout, Conv2D, MaxPooling2D, Conv1D, MaxPooling1D, GlobalAveragePooling1D
from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.utils import to_categorical, normalize
# from tensorflow.python.keras.utils import plot_model
import matplotlib.pylab as plt


import scipy.io as sio

#import logging
#from matplotlib import pyplot as plt
#import os

# Local imports
from pyMEDICOM import utils

#=============================================================================
# DEFS
#=============================================================================

class AccuracyHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.acc = []

    def on_epoch_end(self, batch, logs={}):
        self.acc.append(logs.get('acc'))

def save_object(obj, filename):
    """
    Saves a python object via pickle

    :param obj: object to save
    :param filename: filename
    :type obj: Object
    :type filename: str
    :return: None
    :rtype: None
    """
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def load_object(filename):
    """
    Loads a Python object via pickle

    :param filename: filename
    :type filename: str
    :return: obj
    :rtype: Object
    """

    with open(filename, 'rb') as to_load:
        obj = pickle.load(to_load)
        return obj

#=============================================================================
# MAIN
#=============================================================================

print(utils.getDPI())

# Setup environment
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Just disables the warning, doesn't enable AVX/FMA

# MATLAB load operations
data_dir = '../../data'
data_file = 'data_detection.mat'
data_path = Path(data_dir)
data_full = data_path.joinpath(data_file)

# Model
model_file = 'test_model.h5'
model_full = data_path.joinpath(model_file)

# History
history_acc_file = 'test_history_acc.pkl'
history_acc_full = data_path.joinpath(history_acc_file)

# Load the data
try:
    sio.whosmat(data_full)
    data = sio.loadmat(data_full)
except:
    print('MATLAB data is missing')
# data_path = Path(data_full)
# if not data_path.is_file():

#    batch_size = 128
batch_size = 32
num_classes = 2
epochs = 10
print(model_full)
print(model_full.is_file())
if not model_full.is_file():
    print('Generating model')
    model = Sequential()

    model.add(Conv1D(100, 10, activation='relu', input_shape=(80, 1)))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='sigmoid'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adagrad(),
                  metrics=['accuracy'])

#        plot_model(model,
#                   to_file='model.png',
#                   show_shapes=False,
#                   show_layer_names=True,
#                   rankdir='TB')

    history = AccuracyHistory()

    history.acc = []

    model.save(model_full)
    save_object(history.acc, history_acc_full)
    history_acc = history.acc

else:

    model = load_model(model_full)
    history_acc = load_object(history_acc_full)

print(model.summary())
