# -*- coding: utf-8 -*-
"""
Document: pickle_test.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Test of python's standard library module 'pickle' for saving and loading data

@author: sap625
"""

#=============================================================================
# Imports
#=============================================================================

import pickle

class Company(object):
    def __init__(self, name, value):

        self.name = name
        self.value = value

    def __str__(self):

        print_string = ("{2} - Name: {0}, Value: {1}".format(self.name, \
        self.value, self.__class__.__name__))

        return print_string

def save_object(obj, filename):
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def load_object(filename):
    with open(filename, 'rb') as to_load:
        obj = pickle.load(to_load)
        return obj

# with open('company_data.pkl', 'wb') as output:
#     company1 = Company('banana', 40)
#     pickle.dump(company1, output, pickle.HIGHEST_PROTOCOL)
#
#     company2 = Company('spam', 42)
#     pickle.dump(company2, output, pickle.HIGHEST_PROTOCOL)


company1 = Company('banana', 40)
company2 = Company('spam', 42)
save_object(company1, 'company1.pkl')
save_object(company2, 'company2.pkl')

del company1
del company2

company1 = load_object('company1.pkl')
company2 = load_object('company2.pkl')

print(company1)
print(company2)

# with open('company_data.pkl', 'rb') as input:
#     company1 = pickle.load(input)
#     print(company1.name)  # -> banana
#     print(company1.value)  # -> 40
#
#     company2 = pickle.load(input)
#     print(company2.name) # -> spam
#     print(company2.value)  # -> 42
