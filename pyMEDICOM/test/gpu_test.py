# -*- coding: utf-8 -*-
'''
File: gpu_test.py

Author: Sasha Petrenko

Description:
    This file runs a test to check if the tensorflow-gpu library is loaded

@author: Sasha
'''

#%% IMPORTS

import tensorflow as tf
from tensorflow.python.client import device_lib

#%% FUNCTIONS


def get_available_gpus():
    '''
    Gets a list of available gpus
    '''
    local_device_protos = device_lib.list_local_devices()
    print(local_device_protos)
    return [x.name for x in local_device_protos if x.device_type == 'GPU']


def test_tf_gpu():
    '''
    Tests the ability to run tensorflow with gpus
    '''
    # Creates a graph.
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)

    # Creates a session with log_device_placement set to True.
    config = tf.ConfigProto(allow_soft_placement=True,
                            log_device_placement=True)
    config.gpu_options.allow_growth = True
    #config.gpu_options.per_process_gpu_memory_fraction = 0.4
    sess = tf.Session(config=config)

    # Runs the session and print placements
    print(sess.run(c))


def main():

    # See what gpus are available
    print(get_available_gpus())

    # Test the tensorflow-gpu capability
    test_tf_gpu()

#%% Run if main


if __name__ == '__main__':
    main()
    print('did stuff')
