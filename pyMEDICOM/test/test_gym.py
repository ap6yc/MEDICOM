# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 11:12:07 2018

@author: Sasha
"""

import gym
env = gym.make('CartPole-v0')

env.reset()
for _ in range(100):
    env.render()
    env.step(env.action_space.sample()) # take a random action

# env.render(close=True)
