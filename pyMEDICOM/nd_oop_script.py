# -*- coding: utf-8 -*-
"""
Document: nd_oop.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Performs naive detection of performed maneuvers on angles-only information in an object-oriented way.

@author: sap625
"""

# =============================================================================
# IMPORTS
# =============================================================================

# External imports
# from __future__ import print_function

# Module-level imports
from pyMEDICOM import lib_engine as le

# import os
# import sys

# from pyMEDICOM import lib_engine

# os.chdir(os.path.dirname(__file__))
# print(os.getcwd())
# sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


# from pyMEDICOM import lib_engine as le
# from . import lib_engine as le

# =============================================================================
# MAIN
# =============================================================================

"""Main function

INPUTS: N/A
OUTPUTS: N/A
"""

# Get the engine
mod = le.Engine()

# Load the data
mod.load_data()

# Generate the model
# mod.gen_model()

# Train the model
mod.train()

# Evaluate the model against all data (not
# mod.evaluate()

print('did stuff')
