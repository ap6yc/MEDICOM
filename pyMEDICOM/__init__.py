# -*- coding: utf-8 -*-
"""
Document: __init__.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Identifies this folder as a module

@author: sap625
"""

# Initialization file
