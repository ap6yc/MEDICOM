# -*- coding: utf-8 -*-
"""
Document: hmm_script.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>
Professor: Dr. Donald Wunsch

Description: Script-based implementation of the HMM portion of pyMEDICOM

@author: sap625
"""

# =============================================================================
# IMPORTS
# =============================================================================

# External imports
# from __future__ import print_function
from hmmlearn import hmm

# Module-level imports
from pyMEDICOM import lib_engine as le

# =============================================================================
# MAIN
# =============================================================================

# Get the engine
mod = le.Engine()

# Load the data
mod.load_data_IOD()

# Create the HMM models and fit them to their corresponding data
ma = hmm.GaussianHMM(n_components=1, covariance_type="diag", n_iter=10000).fit(mod.a_data)
mb = hmm.GaussianHMM(n_components=1, covariance_type="diag", n_iter=10000).fit(mod.b_data)

# Predict against the test batch of data
ha = ma.predict(mod.x_test)
hb = mb.predict(mod.x_test)

# Get the final result as a binary classification by comparing the two HMM results
result = [[1] if hb[i] < ha[i] else [0] for i in range(len(mod.y_test))]

# Get the accuracy as a comparison against the test classification data and print
accuracy = sum(1 for x,y in zip(result, mod.y_test) if x == y) / len(result)
print(accuracy)
