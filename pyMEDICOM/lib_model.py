# -*- coding: utf-8 -*-
"""
Document: lib_model.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Implementation of the Model classes for pyMEDICOM

@author: sap625
"""

# =============================================================================
# IMPORTS
# =============================================================================

# External imports
# from __future__ import print_function

# from time import time

from tensorflow.python import keras
from tensorflow.python.keras.layers import Dense, Flatten, Dropout, Conv2D, MaxPooling2D, Conv1D, MaxPooling1D, GlobalAveragePooling1D, BatchNormalization
from tensorflow.python.keras.models import Sequential, load_model
import matplotlib.pylab as plt
# from tensorflow.python.keras.utils import plot_model
# import scipy.io as sio

# Local imports
from pyMEDICOM import utils


# =============================================================================
# AccuracyHistory
# =============================================================================


class AccuracyHistory(keras.callbacks.Callback):
    """Class wrapping accuracy history logging for keras models
    """

    def __init__(self):
        """Initialize the accuracy history
        """

        # self.super()
        super().__init__()

        self.acc = []

        return

    def on_train_begin(self, logs={}):
        """Reinitialize the accuracy history log
        """

        self.acc = []

        return

    def on_epoch_end(self, batch, logs={}):
        """Append the accuracy to the log at the end of each epoch
        """

        self.acc.append(logs.get('acc'))

        return


# =============================================================================
# WrapModel
# =============================================================================

class WrapModel:
    """
    Class containing parameters and operations for a MEDICOM (M) model
    """

    def __init__(self, opts):
        """
        Initialization overload

        :param opts: pyMEDICOM options structure
        :type opts: lib_engine.EngineOpts
        """

        self.opts = opts
        self.model = []

        # Generate the model
        self.model, self.history, self.history_acc = self.get_model()

        return

    def get_model(self):
        """

        :return:
        :rtype:
        """

        if not self.opts.model_full.is_file() or self.opts.regen:
            model = self.gen_model()
            history_acc = []
        else:
            model, history_acc = self.load_model()

        history = AccuracyHistory()
        return model, history, history_acc

    def gen_model(self):
        """Generates (or otherwise loads) the internal model

        :return:
        :rtype:
        """

        print('Generating model')
        model = Sequential()
        model.add(Conv1D(160, 20, activation='relu', input_shape=self.opts.input_shape))
        # model.add(Conv2D(32, kernel_size=(20, 2), strides=(1, 1),
        #           activation='relu',
        #           input_shape=self.opts.input_shape))
        # model.add(GlobalAveragePooling1D())
        model.add(Flatten())
        # model.add(MaxPooling1D())

        # for i in range(2):
        #     model.add(Conv1D(40, 10, activation='relu'))
        #     model.add(MaxPooling1D())

        # model.add(Dropout(0.25))

        # model.add(Flatten())
        # model.add(MaxPooling1D(1))
        # model.add(BatchNormalization())

        # model.add(Conv1D(40, 20, activation='sigmoid'))

        # model.add(GlobalAveragePooling1D())

        # model.add(Dropout(0.25))
        # model.add(BatchNormalization())
        # model.add(Flatten())

        # model.add(MaxPooling1D(10))

        # model.add(Conv1D(40, 5, activation='sigmoid'))
        # model.add(GlobalAveragePooling1D())
        # model.add(Dropout(0.25))

        # model.add(MaxPooling1D(self.opts.num_classes))


        # model.add(Conv2D(32, kernel_size=(10, 2), strides=(1, 1),
        #           activation='relu',
        #           input_shape=self.opts.input_shape))

        # model.add(Dense(self.opts.input_shape, activation='linear', input_dim=self.opts.input_shape))
        # model.add(Dropout(0.25))

        #        model.add(Dense(32, activation='sigmoid'))
        #        model.add(Dropout(0.25))
        #        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        #        model.add(Conv2D(64, (5, 5), activation='relu'))
        #        model.add(MaxPooling2D(pool_size=(2, 2)))
        #        model.add(Flatten())

        # for i in range(1):
        #     model.add(Dense(160, activation='sigmoid'))
        #     model.add(Dropout(0.25))

        for i in range(1):
            model.add(Dense(320, activation='sigmoid'))
            model.add(Dropout(0.25))

        for i in range(1):
            model.add(Dense(160, activation='sigmoid'))
            model.add(Dropout(0.25))

    #        model_m.add(Conv1D(100, 10, activation='relu', input_shape=(TIME_PERIODS, num_sensors)))

        # model.add(Conv1D(80, 10, activation='relu'))
        # model.add(MaxPooling1D(3))
        # model.add(Conv1D(160, 10, activation='relu'))

        # model.add(Conv1D(self.opts.num_classes, 10, activation='relu'))

        # model.add(GlobalAveragePooling1D())

        model.add(Dense(self.opts.num_classes, activation='softmax'))

        # model.add(Dropout(0.25))

        model.compile(loss=keras.losses.categorical_crossentropy,
                     # optimizer=keras.optimizers.Adam(),
                      optimizer=keras.optimizers.Adagrad(),
                     #  optimizer=keras.optimizers.SGD(lr=0.01, momentum=0.9, decay=0.0, nesterov=True),
                      metrics=['accuracy'])

        print(model.summary())

    #        plot_model(model,
    #                   to_file='model.png',
    #                   show_shapes=False,
    #                   show_layer_names=True,
    #                   rankdir='TB')

        return model

    def load_model(self):
        """
        """

        model = load_model(self.opts.model_full)
        history_acc = utils.load_object(self.opts.history_acc_full)

        return model, history_acc

    def train(self, source, target):
        """Trains the model given source data and target data

        :param source: source data for training
        :param target: target data for training
        :type source: numpy.Array
        :return: None
        :rtype: None
        """

        self.model.fit(source, target,
                       batch_size=self.opts.batch_size,
                       epochs=self.opts.epochs,
                       verbose=1,
                       shuffle=True,  #   validation_data=(x_test, y_test),
                       validation_split = self.opts.split,
                       callbacks=[self.history])

        self.model.save(self.opts.model_full)
        self.history_acc = self.history.acc
        utils.save_object(self.history_acc, self.opts.history_acc_full)

        return

    def evaluate(self, source, target):
        """
        """

        # Evaluate and plot
        score = self.model.evaluate(source, target, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        plt.figure()
        plt.plot(range(self.opts.epochs), self.history_acc)
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.show()

        return
