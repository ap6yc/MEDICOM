# -*- coding: utf-8 -*-
"""
Document: lib_engine.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Implementation of the pyMEDICOM engine

@author: sap625
"""

# ============================================================================
# IMPORTS
# =============================================================================

# External imports
# from __future__ import print_function

import os
import sys

import numpy as np
# import matplotlib.pylab as plt
import scipy.io as sio
# from time import time
# from tensorflow.python import keras
from tensorflow.python.keras.utils import to_categorical  # normalize

from pyMEDICOM import utils
from pyMEDICOM import lib_model as lm
from pyMEDICOM.engine_opts import EngineOpts

# =============================================================================
# ENGINE
# =============================================================================


class Engine:
    """
    """

    def __init__(self):
        """Initialize the Engine class
        """

        # Set logging options
        # mpl = mp.log_to_stderr()
        # mpl.setLevel(logging.INFO)

        # Get the options
        self.opts = EngineOpts()

        # Get the model
        self.wrap_model = lm.WrapModel(self.opts)

        # Other fields
        self.data = []
        self.x_data = []
        self.x_train = []
        self.x_test = []

        self.y_data = []
        self.y_train = []
        self.y_test = []
        self.n_data = []

        # Set numpy printing options
        np.set_printoptions(precision=2)

        # Setup environment
        # Just disables the warning, doesn't enable AVX/FMA
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

        return

    def load_data_IOD(self):
        """Loads the data for the IOD experiment
        """
        # Load the data
        try:
            # 'Tm_c', 'Zt_c', 'Zm_c','class'
            print('Loading data in', self.opts.data_full)
            print(sio.whosmat(self.opts.data_full))
            self.data = sio.loadmat(self.opts.data_full)
        except:
            print('MATLAB data is missing')
            sys.exit()

        self.n_data = len(self.data['d'])
        # self.split = 0.8
        # self.test_data = self.data['d'][0:self.n_data]
        self.x_train = self.data['d'][0:int(self.n_data * (1 - self.opts.split))]
        self.x_test = self.data['d'][int(self.n_data * (1 - self.opts.split)):]

        self.y_train = self.data['class'][0:int(self.n_data * (1 - self.opts.split))]
        self.y_test = self.data['class'][int(self.n_data * (1 - self.opts.split)):]

        self.n_train = len(self.x_train)
        self.n_test = len(self.x_test)

        self.a_data = [self.x_train[x] for x in range(self.n_train) if self.y_train[x] == 0]
        self.b_data = [self.x_train[x] for x in range(self.n_train) if self.y_train[x] == 1]
        # self.y_data = to_categorical(np.array(self.data['class']), num_classes=2)
        # self.x_data = np.expand_dims(self.x_data, axis=2)

        # Infer the number of data points
        #

        return


    def load_data(self):
        """Loads the data for the experiment
        """

        # Load the data
        try:
            # 'Tm_c', 'Zt_c', 'Zm_c','class'
            print('Loading data in', self.opts.data_full)
            print(sio.whosmat(self.opts.data_full))
            self.data = sio.loadmat(self.opts.data_full)
        except:
            print('MATLAB data is missing')
            sys.exit()

        # Condition the data
        self.x_data = np.array(self.data['train'])
        print(np.max(self.x_data))
        print(np.min(self.x_data))

        # np.show_config()

        self.x_data = self.x_data / np.pi + 1.0 / 2.0
        print(np.max(self.x_data))
        print(np.min(self.x_data))
        print(self.x_data.shape)

        self.y_data = to_categorical(np.array(self.data['class']), num_classes=2)

        # self.x_data = np.expand_dims(self.x_data, axis=2)

        # Infer the number of data points
        self.n_data = len(self.x_data)

        # Make separate data structures?
        self.x_train = self.x_data[0:int(self.n_data * (1 - self.opts.split))]
        self.x_test = self.x_data[int(self.n_data * (1 - self.opts.split)):]
        # self.x_test = np.expand_dims(self.x_data, axis=2)

        self.y_train = self.y_data[0:int(self.n_data * (1 - self.opts.split))]
        self.y_test = self.y_data[int(self.n_data * (1 - self.opts.split)):]

        self.x_test, self.y_test = utils.unison_shuffled_copies(self.x_test, self.y_test)
        # print('x_train shape:', self.x_train.shape)
        # print('x_test shape:', self.x_test.shape)
        # print('y_train shape:', self.y_train.shape)
        # print('y_test shape:', self.y_test.shape)
        # print(self.n_data, 'train samples')

        return

    def train(self):
        """Trains the embedded keras model

        :return: None
        :rtype: None
        """

        self.wrap_model.train(self.x_data, self.y_data)

        return

    def evaluate(self):
        """Evaluates the internally wrapped model against the test data

        :return: None
        :rtype: None
        """

        self.wrap_model.evaluate(self.x_test, self.y_test)

        return

    # def gen_model(self):
    #     """
    #
    #     :return: None
    #     :rtype: None
    #     """
    #     # Get the model
    #     self.wrap_model = lm.WrapModel(self.opts)
    #
    #     return
