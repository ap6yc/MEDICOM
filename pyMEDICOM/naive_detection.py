# -*- coding: utf-8 -*-
"""
Document: naive_detection.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Performs naive detection of performed maneuvers on angles-only information

@author: sap625
"""

#=============================================================================
# IMPORTS
#=============================================================================

# External imports
# from __future__ import print_function

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import copy
from time import time
import pickle
from pathlib import Path
# sys.path.append(os.path.join(os.path.dirname(__file__), data_dir))

import numpy as np

import tensorflow as tf

from tensorflow.python import keras
#from tensorflow.python.keras.datasets import mnist
from tensorflow.python.keras.layers import Dense, Flatten, Dropout, Conv2D, MaxPooling2D, Conv1D, MaxPooling1D, GlobalAveragePooling1D
from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.utils import to_categorical, normalize
# from tensorflow.python.keras.utils import plot_model
import matplotlib.pylab as plt


import scipy.io as sio

#import logging
#from matplotlib import pyplot as plt
#import os

# Local imports
from pyMEDICOM import utils

#=============================================================================
# DEFS
#=============================================================================

class AccuracyHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.acc = []

    def on_epoch_end(self, batch, logs={}):
        self.acc.append(logs.get('acc'))


def save_object(obj, filename):
    """
    Saves a python object via pickle

    :param obj: object to save
    :param filename: filename
    :type obj: Object
    :type filename: str
    :return: None
    :rtype: None
    """

    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


def load_object(filename):
    """
    Loads a Python object via pickle

    :param filename: filename
    :type filename: str
    :return: obj
    :rtype: Object
    """

    with open(filename, 'rb') as to_load:
        obj = pickle.load(to_load)
        return obj

#=============================================================================
# MAIN
#=============================================================================

def main():
    """Main function

    INPUTS: N/A
    OUTPUTS: N/A
    """

    # Setup environment
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Just disables the warning, doesn't enable AVX/FMA

    # MATLAB load operations
    data_dir = '../data'
    data_file = 'data_detection.mat'
    data_path = Path(data_dir)
    data_full = data_path.joinpath(data_file)

    # Model
    model_file = 'model.h5'
    model_full = data_path.joinpath(model_file)

    # History
    history_acc_file = 'history_acc.pkl'
    history_acc_full = data_path.joinpath(history_acc_file)

    # Load the data
    try:
#    'Tm_c', 'Zt_c', 'Zm_c','class'
        sio.whosmat(data_full)
        data = sio.loadmat(data_full)
    except:
        print('MATLAB data is missing')
    # data_path = Path(data_full)
    # if not data_path.is_file():

    batch_size = 128
#    batch_size = 32
    num_classes = 2
    epochs = 10
    input_shape = 80

#    print(data['train'])
#    x_data = np.array(data['train'])
    x_data = np.array(data['train'])
    x_data = x_data / np.pi
#    print(x_data)
#    x_data = normalize(x_data, axis=1, order = 2)

    n_data = len(x_data)
#    print(n_data)
    p_train = 0.8

    x_train = x_data[0:int(n_data*p_train)]

    x_test = x_data[int(n_data*p_train):]

#    x_test = np.expand_dims(x_test, axis=2)

#    x_train = normalize(data, 1, order=2)
    y_data = to_categorical(np.array(data['class']), num_classes=num_classes)

    y_train = y_data[0:int(n_data*p_train)]

    y_test = y_data[int(n_data*p_train):]

    print('x_train shape:', x_train.shape)
    print('x_test shape:', x_test.shape)
    print('y_train shape:', y_train.shape)
    print('y_test shape:', y_test.shape)
    print(n_data, 'train samples')

    if not model_full.is_file():
        print('Generating model')

        model = Sequential()

#        x_data = np.expand_dims(x_data, axis=2)

#        model.add(Conv1D(80, 10, activation='linear', input_shape=(80, 1)))

#        model.add(Conv2D(32, kernel_size=(2, 5), strides=(1, 1),
#                         activation='relu',
#                         input_shape=input_shape))

        model.add(Dense(input_shape, activation='linear', input_dim=input_shape))
#        model.add(Dropout(0.5))
        model.add(Dense(160, activation='sigmoid'))

#        model.add(Dropout(0.25))
#        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
#        model.add(Conv2D(64, (5, 5), activation='relu'))
#        model.add(MaxPooling2D(pool_size=(2, 2)))
#        model.add(Flatten())
#        model.add(Dense(1000, activation='relu'))
#        model.add(Dense(num_classes, activation='softmax'))

#        model.add(Dense(32, activation='sigmoid'))
#        model.add(Dropout(0.25))
#        model.add(Dense(32, activation='sigmoid'))
#        model.add(Dropout(0.25))

#        for i in range(10):
#            model.add(Dense(100, activation='sigmoid'))

#        model_m.add(Conv1D(100, 10, activation='relu', input_shape=(TIME_PERIODS, num_sensors)))
#        model.add(Conv1D(80, 10, activation='linear'))
#        model.add(MaxPooling1D(3))
#        model.add(Conv1D(160, 10, activation='linear'))
#        model.add(Conv1D(160, 10, activation='linear'))
#        model.add(GlobalAveragePooling1D())
#        model.add(Dropout(0.5))
#        model.add(Flatten())
        model.add(Dense(160, activation='linear'))
        model.add(Dense(80, activation='linear'))
#        model.add(Dropout(0.25))
        model.add(Dense(num_classes, activation='sigmoid'))


#        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.binary_crossentropy,
#                      optimizer=keras.optimizers.Adam(),
#                      optimizer=keras.optimizers.Adagrad(),
                      optimizer=keras.optimizers.Adadelta(),
                      metrics=['accuracy'])

        print(model.summary())

#        plot_model(model,
#                   to_file='model.png',
#                   show_shapes=False,
#                   show_layer_names=True,
#                   rankdir='TB')

        history = AccuracyHistory()

#        model.fit(x_train, y_train,
        model.fit(x_data, y_data,
                  batch_size=batch_size,
                  epochs=epochs,
                  verbose=1,
                  shuffle=True,
#                  validation_data=(x_test, y_test),
                  validation_split = 0.2,
                  callbacks=[history])

        model.save(model_full)
        save_object(history.acc, history_acc_full)
        history_acc = history.acc

    else:

        model = load_model(model_full)
        history_acc = load_object(history_acc_full)

    # Evaluate and plot
    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    local_dpi = utils.getDPI()

    plt.figure(num = 1, dpi=local_dpi)

    plt.plot(range(epochs), history_acc)
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.show()

    return

#=============================================================================
# RUN MAIN
#=============================================================================

if __name__ == '__main__':
    main()
    print('did stuff')
