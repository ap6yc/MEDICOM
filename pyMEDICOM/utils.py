# -*- coding: utf-8 -*-
"""
Document: utils.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Utilities, functions, etc.

@author: sap625
"""

# =============================================================================
# Imports
# =============================================================================

# External imports
import tkinter as tk
import pickle
import numpy as np

# import matplotlib.pyplot as plt
# import multiprocessing as mp
# from sys import exit

# =============================================================================
# Functions
# =============================================================================


def save_object(obj, filename):
    """
    Saves a python object via pickle

    :param obj: object to save
    :param filename: filename
    :type obj: Object
    :type filename: str
    :return: None
    :rtype: None
    """
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def load_object(filename):
    """
    Loads a Python object via pickle

    :param filename: filename
    :type filename: str
    :return: obj
    :rtype: Object
    """

    with open(filename, 'rb') as to_load:
        obj = pickle.load(to_load)
        return obj


def getDPI():
    """
    Gets the DPI of the current system for consistent window resolutions.

    This is especially important for switching to high DPI systems.

    :return: width_dpi - dpi as inferred from the horizontal calculation
    :rtype: float
    """

    # Access the graphic user interface engine
    root = tk.Tk()

    width_px = root.winfo_screenwidth()
    height_px = root.winfo_screenheight()
    width_mm = root.winfo_screenmmwidth()
    height_mm = root.winfo_screenmmheight()
    # 2.54 cm = in
    width_in = width_mm / 25.4
    height_in = height_mm / 25.4
    width_dpi = width_px/width_in
    height_dpi = height_px/height_in

    print('Width: %i px, Height: %i px' % (width_px, height_px))
    print('Width: %i mm, Height: %i mm' % (width_mm, height_mm))
    print('Width: %.2f in, Height: %.2f in' % (width_in, height_in))
    print('Width: %.2f dpi, Height: %.2f dpi' % (width_dpi, height_dpi))

#        self.dpi = width_dpi
#        user32 = ctypes.windll.user32
#        user32.SetProcessDPIAware()
#        [w, h] = [user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)]
#        print('Size is %f %f' % (w, h))
#
#        curr_dpi = w*96/width_px
#        print('Current DPI is %f' % (curr_dpi))
#
#        print(user32.GetSystemMetrics(3))

    return int(width_dpi)


def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]