# -*- coding: utf-8 -*-
"""
Document: engine_opts.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Implementation of the pyMEDICOM engine

@author: sap625
"""


# =============================================================================
# IMPORTS
# =============================================================================


# External imports

import os
from pathlib import Path

import yaml
# Local Imports
from pyMEDICOM import utils


# ============================================================================
# ENGINEOPTS
# =============================================================================

def load_yaml(yaml_filepath):
    """Load a YAML file.
    """

    with open(yaml_filepath, 'r') as stream:
        opts = yaml.load(stream)

    return opts


def save_yaml(yaml_filepath, data):
    """Save a YAML file
    """

    with open(yaml_filepath, 'w') as stream:
        yaml.dump(data, stream=stream)

    return


class EngineOpts:
    """
    """

    def __init__(self):
        """Implementation of the pyMEDICOM Engine Options structure
        """

        self.batch_size = 32    # 128
        self.num_classes = 2
        self.epochs = 10
        self.split = 0.2
        # self.input_shape = 80
        self.input_shape = (40, 2)

        # Top-level options
        self.regen = True

        # Names
        self.data_dir = '../data'
        self.detection_dir = 'detection'
        self.model_file = 'model.h5'
        self.history_acc_file = 'history_acc.pkl'
        self.opts_file = 'opts.yml'

        # MATLAB load operations

        self.data_path = Path(self.data_dir)

        # Options
        self.opts_full = self.data_path.joinpath(self.opts_file)

        # Data
        self.detection_path = self.data_path.joinpath(self.detection_dir)

        # Get a time-sorted list of the data files
        data_files = sorted([file for file in self.detection_path.glob('*.mat')], key=os.path.getmtime)

        # Identify the most recent MATLAB file
        self.data_full = data_files[-1]

        # Model
        self.model_full = self.data_path.joinpath(self.model_file)

        # History
        self.history_acc_full = self.data_path.joinpath(self.history_acc_file)

        print('Using dpi = ', utils.getDPI())

        self.opts = load_yaml(self.opts_full)

        # print(self.opts)

        # yaml.dump(self)
        # save_yaml(self.opts_full, self)

        return

