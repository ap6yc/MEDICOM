# -*- coding: utf-8 -*-
"""
Document: nd_oop.py

Author: Sasha Petrenko <sap625@mst.edu>
Advisor: Dr. Kyle DeMars <demarsk@mst.edu>

Description: Performs naive detection of performed maneuvers on angles-only information in an object-oriented way.

@author: sap625
"""

#=============================================================================
# IMPORTS
#=============================================================================

# External imports
# from __future__ import print_function

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

# Local imports
from pyMEDICOM import lib_engine as le


#=============================================================================
# MAIN
#=============================================================================

def main():
    """Main function

    INPUTS: N/A
    OUTPUTS: N/A
    """

    # Get the engine
    mod = le.Engine()

    # Load the data
    mod.loadData()

    # Train the model
    mod.train()

#=============================================================================
# RUN MAIN
#=============================================================================

if __name__ == '__main__':
    main()
    print('did stuff')
