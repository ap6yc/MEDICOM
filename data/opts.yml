# -----------------------------------------------------------------------------
# File: opts.yml
# Author: Sasha Petrenko
# Date: 11/16/2018
# Description: This YAML file contains some global configuration options for the MEDICOM project
# Requirements:
# -----------------------------------------------------------------------------

# Classification local variables to save
to_save:
  - 'Zmfp1'
  - 'Tmfp1'
  - 'Rtfp1'
  - 'Zm'
  - 'Tm'
  - 'Rm'
  - 'Xt'
  - 'Rt'
  - 'class_IOD'

# These options get expanded in the MATLAB simulation.
# Make sure that these names do not collide with others in this file.
# This is done because the context-level options were not written in the
# original version of the simulation, and mass-refactoring sucks.
MATLAB:
  # Folders that are recursively included on the MATLAB path
  folders:
    - 'utilities'
    - 'functions'
    - 'scripts'
    - 'classes'
    - 'surfmaps'

  # FLAGS - true/yes , false/no
  flags:
    IOD: yes
    SIMPLE_PROP: yes
    WARNINGS: yes
    PLOT: no                # Global plotting flag
    CONSOLE_FILTER: no      # Filter console out
    RUN_PARALLEL: yes       # Run in parallel
    CLOSE_POOL: no          # Close the parallel pool after running
    PERTURBED: yes          # Run with perturbations
    CLOUDS: no              # Run with clouds effects on night
    PLOT_DIAGNOSTICS: true  # Diagnostic plotting flag

  # SWITCHES
  switches:
    LOG_LEVEL: 'INFO'         # TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    LOG_WINDOW_LEVEL: 'INFO'  # TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    MAN_DIRECTION: 'PLANAR'   # PLANAR, RANDOM
    MAN_MAGNITUDE: 'FIXED'    # FIXED, RANDOM

  # Configuration/options

  # Simulation propagation options
  t0: 0.0     # Seconds
  dt: 60.0    # Seconds
  tf: 1.0    # Days
  n_runs: 1 # Number of runs starting at initial conditions
  n_cut: 20   # Number of points to cut (raw data case)

  # Simulation options
  noise_mode: 'NOMINAL'     # FAIL, NOMINAL
  save_mode: 'DETECTION'    # HW07, DETECTION
  rand_seed: 123

  # TLE data retrieved from celestrak.net at 2000 on 10/03/17
  # GPS BIIF-5  (PRN 30)
  TLE_1: '1 39533U 14008A   17276.24151446  .00000017  00000-0  00000+0 0  9993'
  TLE_2: '2 39533  54.2433 279.4267 0030779 189.3241 170.6439  2.00569339 26473'

  # Dynamics options
  dv_max: 1e-3

  # MECHANICAL OPTIONS
  date_format: '_dd-mmm-yyyy_HH-MM-SS'  # Date format for save-name generation
  max_retry_count: 2                   # Parallel options
  # Logger options
  log_path: 'logs/'
  # Name "templates"
  save_template: 'data_detection'


# SPICE function paths and data loading
mice_path: '../SPICE/mice/'   # SPICE mice path
kernels_path: '../kernels/'   # SPICE kernels path
data_path: '../data'          # Local data path
detection_dir: 'detection'
copy_path: 'S:\dev\data'      # Explicit data copy path

# Conversion constants
#GM  : 3.986004415e5      % [km^3/s^2]
#req : 6378.137           % [km] WGS84 value
#omg : 7.2921151467064e-5 % [rad/s] IERS value
#
## Conversion constants
#asc2deg: 1.0/3600.0
#deg2rad: pi/180.0
#asc2rad: asc2deg*deg2rad
#rad2asc: 3600.0*180.0/pi
#rad2deg: 180.0/pi
#dy2hr  : 24.0
#hr2mn  : 60.0
#mn2sc  : 60.0
#dy2sc  : 86400.0
#hr2sc  : 3600.0


#    % "Options options" (all under opts.data_path)
#    opts.opts_name = 'opts.yml';
#    opts.inst_opts_path = 'configs';
#    opts.inst_opts_name = 'opts';      % Name of the enumerated configuration

#    % NOTE: opts.to_save is driven by the below and switched by
#    opts.to_save_hw     = {'Tm','Zm','Rm','Xt','Rt'}; % HW local variables to save
#%     opts.to_save_class  = {'Tm_c', 'Zt_c', 'Zm_c','train','class'}; % Classification local variables to save
#    opts.to_save_class  = {'train','class'}; % Classification local variables to save
#
#    % YAML options loading
#    opts.config = ReadYaml(opts.opts_full);
#
#    % Switch between save modes
#    switch opts.save_mode
#        case 'HW07'
#            opts.to_save = opts.to_save_hw;
#        case 'DETECTION'
#            opts.to_save = opts.to_save_class;
#    end
#    % Write current configuration for future reproduction
#    WriteYaml(opts.inst_opts_full, opts_to_save);