function [data, stats] = dol_IOD(job, opts, in_data)
%DOL Division of labor function, running jobs in parallel or serial
%   INPUTS:
%       job - function handle for a single job instance
%       opts - options struct
%       in_data - input data to the job
%   OUTPUTS:
%       data - cell array of data from each run
%       stats - struct containing statistics from the runs

% Iterate against each individual
data = cell(opts.n_runs, 1);
timers = cell(opts.n_runs, 1);

opts.logger.info(mfilename, ['Beginning DOL loop for job:', func2str(job)]);

n_runs_local = length(in_data);

if opts.RUN_PARALLEL
    
    % Add counting and failure flag for the parallel loop
    count = 0;
    flag_failure = 1;
    
    while flag_failure
        
        try
            
            opts.logger.info(mfilename, 'Beginning parallel loop');

            parfor i = 1:n_runs_local
                try
                    [data{i}, timers{i}] = feval(job, opts, i, in_data{i});
                catch
                    % Log and warn
                    message = ['Parallel job ', num2str(i),' failed, passing'];
                    opts.logger.warn(mfilename, message);
                    warning(message)
                end
            end
            
            % Reset the failure flag
            flag_failure = 0;
        catch
            
            % Log and warn
            message = 'Parallel job failed, restarting';
            opts.logger.warn(mfilename, message);
            warning(message)
            
            % Increment and check if max retry count is reached
            count = count + 1;
            if count > opts.max_retry_count
                break
            end
        end
    end
    
    % Report failure
    if flag_failure
        message = 'Complete generation failure';
        opts.logger.warn(mfilename, message)
        warning(message)
    end % if flag_failure
    
% Else, run serially
else
    % Add counting and failure flag for the serial loop
    count = 0;
    flag_failure = 1;
    
    while flag_failure
        
        try
            
            opts.logger.info(mfilename, 'Beginning serial loop');
            
            for i = 1:n_runs_local
                
                [data{i}, timers{i}] = job(opts, i, in_data);
                
            end
            
        catch
            
            % Log and warn
            message = ['Serial job ', num2str(i),' failed, restarting'];
            opts.logger.warn(mfilename, message);
            warning(message)
            
            % Increment and check if max retry count is reached
            count = count + 1;
            if count > opts.max_retry_count
                break
            end
            
        end
        
    end % while flag_failure
    
    % Report failure
    if flag_failure
        message = 'Complete generation failure';
        opts.logger.warn(mfilename, message);
        warning(message)
    end
    
end % if opts.RUN_PARALLEL

% Stats compilation
stats.timers = timers;

end