function [data, timer] = job_gen_data(opts, i)

    % Time the local iteration
    local_timer = tic;

    % Create a separate logger for the parallel instance
    logger_local = opts.get_logger(['P', num2str(i)]);

    % Furnish for this job
    furnish(opts, logger_local);
    
    % Gen that data
    logger_local.info(mfilename, ['Entering data generation for iteration ', num2str(i)]);
    data = f_gen_data(opts, logger_local);
    logger_local.info(mfilename, ['Ending data generation for iteration ', num2str(i)]);

    % End the timer, store the time
    timer = toc(local_timer);

end