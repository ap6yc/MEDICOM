function [xhat, P] = f_IOD(in_data, opts, logger)
%IOD 
%     iIOD = [1;61;122];
%     tIOD = Tm(iIOD);
%     aIOD = Zm(1,iIOD)*c.asc2deg; % [arcsec] -> [deg]
%     dIOD = Zm(2,iIOD)*c.asc2deg; % [arcsec] -> [deg]
%     rIOD = Rt(:,iIOD);
%     
%     % IOD on each arc
%     if opts.IOD
%         % apply Gauss' method with Gibbs' method,,,
%     %     [t2,x2] = IODgauss(tIOD,aIOD,dIOD,rIOD,c.GM,'Gibbs');
%         [t2,x2] = IODgauss(T, A, D, R, c.GM, method);
%     end

%     % Constants
%     c.GM   = 3.986004415e5;      % [km^3/s^2]
%     c.req  = 6378.137;           % [km] WGS84 value
%     c.omg  = 7.2921151467064e-5; % [rad/s] IERS value
% 
%     % Conversion constants
%     c.asc2deg = 1.0/3600.0;
%     c.deg2rad = pi/180.0;
%     c.asc2rad = c.asc2deg*c.deg2rad;
%     c.rad2asc = 3600.0*180.0/pi;
%     c.rad2deg = 180.0/pi;
%     c.dy2hr   = 24.0;
%     c.hr2mn   = 60.0;
%     c.mn2sc   = 60.0;
%     c.dy2sc   = 86400.0;
%     c.hr2sc   = 3600.0;
%     T, A, D, R, c.GM
    
    % Get constants as a local struct
    c = get_constants();

    %%% Problem 1
    if opts.CONSOLE_FILTER
        disp('Problem #1')
    end

    % Perform initial orbit determination via Gauss' method with Gibbs' method
    % extract three measurements (time, RA, DEC, inertial position of station)
    % we're using data points 1, 61, and 122 for IOD
%     iIOD = [1;61;122];
%     tIOD = Tm(iIOD);
%     aIOD = Zm(1,iIOD)*c.asc2deg; % [arcsec] -> [deg]
%     dIOD = Zm(2,iIOD)*c.asc2deg; % [arcsec] -> [deg]
%     rIOD = Rt(:,iIOD);
    n_data = length(in_data.Tm);
    iIOD = [1, floor(n_data/2), n_data];
    tIOD = in_data.Tm(iIOD);
    aIOD = in_data.Zm(1,iIOD)*c.asc2deg; % [arcsec] -> [deg]
    dIOD = in_data.Zm(2,iIOD)*c.asc2deg; % [arcsec] -> [deg]
    rIOD = in_data.Rt(:,iIOD);

    % apply Gauss' method with Gibbs' method,,,
    [t2,x2] = IODgauss(tIOD,aIOD,dIOD,rIOD,c.GM,'Gibbs');

    % compute pos/vel err at t2
    if opts.CONSOLE_FILTER
        rerr_gauss_t2 = norm(x2(1:3) - Xt(1:3,iIOD(2)));
        verr_gauss_t2 = 1e3*norm(x2(4:6) - Xt(4:6,iIOD(2)));
        disp('   IOD at t2')
        disp(['      pos error = ',num2str(rerr_gauss_t2,'%-.3f'),' km'])
        disp(['      vel error = ',num2str(verr_gauss_t2,'%-.3f'),' m/s'])
    end

    % -------------------------------------------------------------------------



    % -------------------------------------------------------------------------
    %%% Problem 2
    if opts.CONSOLE_FILTER
        disp(' ')
        disp('Problem #2')
    end

    % propagate Gauss solution at t2 back to t1
    opt    = odeset('AbsTol',1e-9,'RelTol',1e-9);
    [~,XX] = ode45(@eom_car, [t2, tIOD(1)], x2, opt,c.GM);
    t1     = tIOD(1);
    x1     = XX(end,:)';

    % Perform iterative improvement via batch least squares (LUMVE)
    % set the number of measurements to use
    % we're using all of the data in the first two arcs
%     kmax  = 241;
    kmax = n_data;  % All of the data of the first (and only) arc

    % set the epoch time, reference state, initial deviation, and covariance
    t0    = t1;
    x0ref = x1;
    x0    = zeros(6,1);

    % iterate until the pos changes are < 100 m and the vel changes are < 100 mm/s
    pbnd = 0.1;
    vbnd = 1e-4;
    imax = 4;

    % begin the iteration loop
    exit = false;
    iter = 0;

    % Integration options
    int_opts   = odeset('AbsTol',1e-9,'RelTol',1e-9);

    while(~exit)
        % update iteration counter
        iter = iter + 1;

        % no prior information, so Lambda and lambda are both zero
        Lam  = zeros(6,6);
        lam  = zeros(6,1);

        % initialize the reference state and the STM
        xref = x0ref;
        Phi  = eye(6);

        % propagate the reference state and STM across the entire timespan

        [~,XX] = ode45(@eom_tbp_ref,in_data.Tm(1:kmax),[xref;Phi(:)],int_opts,c.GM);

        % declare storage for the time, index, and residual
        rest = zeros(kmax,1);
        resi = zeros(kmax,1);
        resm = zeros(kmax,2);
        for k = 1:kmax
            % extract the time, measurement, covariance, and station position for the kth observation
            tk = in_data.Tm(k);
            zk = in_data.Zm(:,k);
            Rk = in_data.Rm(:,:,k);
            rk = in_data.Rt(:,k);

            % determine the LUMVE weighting matrix
            Ri = inv(Rk);

            % unpack the reference state and STM
            xref   = XX(k,1:6)';
            Phi    = reshape(XX(k,7:end)',6,6);

            % compute the reference state measurement (RA and DEC)
            %   form the relative position, then compute the ref. measurement
            %   change the units of the reference measurement to [arcsec]
            rosi  = xref(1:3) - rk;
            x     = rosi(1);
            y     = rosi(2);
            z     = rosi(3);
            wsq   = x*x + y*y;
            w     = sqrt(wsq);
            rhosq = wsq + z*z;
            zref  = [atan2(y,x);atan2(z,w)];
            zref  = zref*c.rad2asc;

            % compute the measurement mapping matrix (Htilde)
            %   change the units of the Jacobian to [arcsec]
            Ht = [        -y/wsq,          x/wsq,     0.0, 0.0, 0.0, 0.0;
                  -x*z/(w*rhosq), -y*z/(w*rhosq), w/rhosq, 0.0, 0.0, 0.0];
            Ht = Ht*c.rad2asc;

            % time-mapping of the observation matrix
            H = Ht*Phi;

            % accumulate the lambdas for LUMVE
%             lam = lam + H'*Ri*(zk - zref);
            lam = lam + H\Ri*(zk - zref);
%             Lam = Lam + H'*Ri*H;
            Lam = Lam + H\Ri*H;

            % store the time, index, and residual for plotting later
            rest(k)   = tk;
            resi(k)   = k;
            resm(k,:) = zk - zref;
        end
        % get the least squares solution
        delx = Lam\lam;
%         disp(delx)
        % perform the iteration by shifting the reference and the estimated deviation
        x0ref = x0ref + delx;
        x0    = x0 - delx;

        % plot the measurement residuals for right-ascension on subplots
        if opts.PLOT
            figure(10)
            C = get(gca,'ColorOrder');
            subplot(imax,1,iter)
            plot(resi,resm(:,1),'x','Color',C(iter,:),'LineWidth',1.2,'MarkerSize',5)
            ylabel(['Iter \#',int2str(iter)])

            % plot the measurement residuals for declination on subplots
            figure(20)
            C = get(gca,'ColorOrder');
            subplot(imax,1,iter)
            plot(resi,resm(:,2),'x','Color',C(iter,:),'LineWidth',1.2,'MarkerSize',5)
            ylabel(['Iter \#',int2str(iter)])
        end

        % compute position/velocity deltas
        pdelt = norm(delx(1:3));
        vdelt = norm(delx(4:6));

        % check for convergence
        if(pdelt <= pbnd && vdelt <= vbnd)
            exit = true;
            break
        end
    end

    if opts.CONSOLE_FILTER
        disp(['   Batch least-squares converged in ',int2str(iter),' iterations'])
    end

    if opts.PLOT
        figure(10)
        xlabel('Measurement Index')
        subplot(imax,1,1)
        title('Right-Ascension Residuals at Each Iteration [arcsec]')

        figure(20)
        xlabel('Measurement Index')
        subplot(imax,1,1)
        title('Declination Residuals at Each Iteration [arcsec]')
    end

    % determine the estimated state and covariance
    xhat = x0ref;
    P    = inv(Lam);

    if opts.CONSOLE_FILTER
        % compute pos/vel err at t1
        rerr_LSQ_t1 = 1e3*norm(xhat(1:3) - Xt(1:3,iIOD(1)));
        verr_LSQ_t1 = 1e6*norm(xhat(4:6) - Xt(4:6,iIOD(1)));
        disp('   Batch least-squares at t1')
        disp(['      pos error = ',num2str(rerr_LSQ_t1,'%-.3f'),' m'])
        disp(['      vel error = ',num2str(verr_LSQ_t1,'%-.3f'),' mm/s'])
        % compute pos/vel std at t1
        rsig_LSQ_t1 = 1e3*sqrt(diag(P(1:3,1:3)));
        vsig_LSQ_t1 = 1e6*sqrt(diag(P(4:6,4:6)));
        disp(['      pos x sigma = ',num2str(rsig_LSQ_t1(1),'%-.3f'),' m'])
        disp(['      pos y sigma = ',num2str(rsig_LSQ_t1(2),'%-.3f'),' m'])
        disp(['      pos z sigma = ',num2str(rsig_LSQ_t1(3),'%-.3f'),' m'])
        disp(['      vel x sigma = ',num2str(vsig_LSQ_t1(1),'%-.3f'),' mm/s'])
        disp(['      vel y sigma = ',num2str(vsig_LSQ_t1(2),'%-.3f'),' mm/s'])
        disp(['      vel z sigma = ',num2str(vsig_LSQ_t1(3),'%-.3f'),' mm/s'])
    end
    
end