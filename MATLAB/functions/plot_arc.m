function plot_arc(data)
%PLOT_ARC Summary of this function goes here
%   Detailed explanation goes here

c = get_constants()

lw = 3;

figure

subplot(2,1,1)
plot(data.Tm, data.Zm(1,:)*c.asc2deg, 'LineWidth', lw)
hold on; grid on;
title('Sample Arc')
ylabel('Right Ascension \alpha [deg]');
a = gca;
a.LineWidth = lw;

subplot(2,1,2)
plot(data.Tm, data.Zm(2,:)*c.asc2deg,'LineWidth', lw)
hold on; grid on;
ylabel('Declination \delta [deg]');
xlabel('Time [s]')
a = gca;
a.LineWidth = lw;

saveas(gcf, '../sample_arc.png')

% figure()
% C = get(gca,'ColorOrder');
% subplot(imax,1,iter)
% plot(resi,resm(:,1),'x','Color',C(iter,:),'LineWidth',1.2,'MarkerSize',5)
% ylabel(['Iter \#',int2str(iter)])
% 
% % plot the measurement residuals for declination on subplots
% figure(20)
% C = get(gca,'ColorOrder');
% subplot(imax,1,iter)
% plot(resi,resm(:,2),'x','Color',C(iter,:),'LineWidth',1.2,'MarkerSize',5)
% ylabel(['Iter \#',int2str(iter)])
end

