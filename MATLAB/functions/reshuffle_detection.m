function train_out = reshuffle_detection(train)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    
    [m, n] = size(train);
    
    train_out = zeros(m, n/2, 2);
    
    for i = 1:m
        train_out(i, :, 1) = train(i, 1:n/2);
        train_out(i, :, 2) = train(i, n/2 + 1:end);
    end
    
end

