function c = get_constants()
%GET_CONSTANTS Summary of this function goes here
%   Detailed explanation goes here

    % Constants
    c.GM   = 3.986004415e5;      % [km^3/s^2]
    c.req  = 6378.137;           % [km] WGS84 value
    c.omg  = 7.2921151467064e-5; % [rad/s] IERS value

    % Conversion constants
    c.asc2deg = 1.0/3600.0;
    c.deg2rad = pi/180.0;
    c.asc2rad = c.asc2deg*c.deg2rad;
    c.rad2asc = 3600.0*180.0/pi;
    c.rad2deg = 180.0/pi;
    c.dy2hr   = 24.0;
    c.hr2mn   = 60.0;
    c.mn2sc   = 60.0;
    c.dy2sc   = 86400.0;
    c.hr2sc   = 3600.0;
    
end

