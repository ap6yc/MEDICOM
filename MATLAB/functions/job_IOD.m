function [data, timer] = job_IOD(opts, i, in_data)
    
    % Time the local iteration
    local_timer = tic;

    % Create a separate logger for the parallel instance
    logger_local = opts.get_logger(['P', num2str(i)]);
    
    % IOD that data
    logger_local.info(mfilename, ['Entering IOD for parallel iteration ', num2str(i)]);
    data = f_IOD_min(in_data, opts, logger_local);
    logger_local.info(mfilename, ['Ending IOD for parallel iteration ', num2str(i)]);

    % End the timer, store the time
    timer = toc(local_timer);

end