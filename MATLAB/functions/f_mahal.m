function d = f_mahal(x, mu, P)
%F_MAHAL Summary of this function goes here
%   d = sqrt( (x - mu)' S^{-1} (x - mu)
    d = sqrt((x - mu)' / inv(P) * (x - mu));
end

