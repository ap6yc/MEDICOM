function opts = genOpts()
%GENOPTS Generates the options for generating MEDICOM data
%   INPUTS:
%       N/A
%   OUTPUTS:
%       opts - MEDICOM options struct
%   ASSUMES:
%       YAML - MATLAB yaml parsing and dumping utility
    
    % SPICE function paths and data loading
    opts.mice_path    = '../SPICE/mice/';
    opts.kernels_path = '../kernels/';
    opts.data_path    = '../data';

    % Logger options
    opts.log_path  = 'logs/';
    
    % "Options options" (all under opts.data_path)
    opts.opts_name = 'opts.yml';
    opts.inst_opts_path = 'configs';
    opts.inst_opts_name = 'opts';      % Name of the enumerated configuration
    
    % Configuration/options
    
    % FLAGS - true, false
    opts.PLOT             = false;      % Global plotting flag
    opts.CONSOLE_FILTER   = false;      % Filter console out
    opts.RUN_PARALLEL     = true;       % Run in parallel
    opts.CLOSE_POOL       = false;      % Close the parallel pool after running
    opts.PERTURBED        = true;       % Run with perturbations
    opts.CLOUDS           = false;      % Run with clouds effects on night
    
    % SWITCHES
    opts.LOG_LEVEL        = 'INFO';     % TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    opts.LOG_WINDOW_LEVEL = 'INFO';     % TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    opts.MAN_DIRECTION    = 'PLANAR';   % PLANAR, RANDOM
    opts.MAN_MAGNITUDE    = 'FIXED';    % FIXED, RANDOM
    opts.PLOT_DIAGNOSTICS = true;
    
    % Dynamics options
    opts.dv_max         = 1e-3;
    
    % Delta-V model
    opts.dv = @(x) dV(x, opts);
    
    % Simulation propagation options
    opts.t0 =  0.0;     % seconds
    opts.dt = 10.0;     % seconds
    opts.tf = 10.0;     % days
                           
    % Simulation options
    opts.noise_mode = 'NOMINAL';         % FAIL, NOMINAL
    opts.save_mode  = 'DETECTION';       % HW07, DETECTION
    opts.rand_seed  = 123;
    
    % NOTE: opts.to_save is driven by the below and switched by
    opts.to_save_hw     = {'Tm','Zm','Rm','Xt','Rt'}; % HW local variables to save
%     opts.to_save_class  = {'Tm_c', 'Zt_c', 'Zm_c','train','class'}; % Classification local variables to save
    opts.to_save_class  = {'train','class'}; % Classification local variables to save
    opts.date_format = '_dd-mmm-yyyy_HH-MM-SS';
    
    opts.rand_seed      = 100;
    opts.n_runs         = 100;
    opts.n_cut          = 20;
    
    % TLE data retrieved from celestrak.net at 2000 on 10/03/17
    % GPS BIIF-5  (PRN 30)    
    opts.TLE(1,:) = '1 39533U 14008A   17276.24151446  .00000017  00000-0  00000+0 0  9993';
    opts.TLE(2,:) = '2 39533  54.2433 279.4267 0030779 189.3241 170.6439  2.00569339 26473';

    % DERIVATIVE OPTIONS
    
    % Full naming options
    opts.save_file  = ['data_detection', datestr(now, '_dd-mmm-yyyy_HH-MM-SS')]; % Name of final data file
    opts.save_full = [opts.data_path, '/', opts.save_file];
    opts.inst_opts_full = [opts.data_path, '/', opts.inst_opts_path, '/', ...
        opts.inst_opts_name, datestr(now, opts.date_format), '.yml'];
    opts.opts_full = [opts.data_path, '/', opts.opts_name];
    
    % MECHANICAL OPTIONS
    
    % YAML options loading
    opts.config = ReadYaml(opts.opts_full);
    
    % Logger generator function
    opts.get_logger = @(pre) get_logger(pre, opts);
    
    % Parallel options
    opts.max_retry_count = 10;
    
    % Switch between save modes
    switch opts.save_mode
        case 'HW07'
            opts.to_save = opts.to_save_hw;
        case 'DETECTION'
            opts.to_save = opts.to_save_class;
    end
    
    % Logger location
    opts.logger = opts.get_logger('main');
    
    % Carve out function handles and save the options configuration
    % TODO: Do this literally any other way
    opts_to_save = opts;
    to_remove = {'get_logger', 'dv','save_file','save_full',...
        'inst_opts_full','opts_full','logger','TLE'};
    opts_to_save = rmfield(opts_to_save, to_remove);
    
    % Write current configuration for future reproduction
    WriteYaml(opts.inst_opts_full, opts_to_save);
    
end

