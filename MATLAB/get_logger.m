function logger = get_logger(pre, opts)
%GET_LOGGER
% asdf
%   INPUTS:
%       pre  - prepended name to uniquely identify the logger's file
%       opts - MEDICOM options structure
%   OUTPUTS:

    name = [opts.log_path,'log_', pre, datestr(now, '_dd-mmm-yyyy_HH-MM-SS'), '.txt'];
    
    % Generate the logger
    logger = log4m.getLogger(name);
    
    % Set the logger options to those simulation-wide
    logger.setCommandWindowLevel(logger.(opts.LOG_WINDOW_LEVEL));
    logger.setLogLevel(logger.(opts.LOG_LEVEL));

    % Logger level
    % TRACE, DEBUG, INFO, WARN, ERROR, FATAL

    % To log an error event
    % opts.logger.error('exampleFunction','An error occurred');

    % To log a trace event
    % opts.logger.trace('function','Trace this event');
end