% -------------------------------------------------------------------------
% File: context.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 11/20/2018
% Description:
% 
%   This script sets up the environment for the MATLAB data generation
%   component of the MEDICOM project. 
% 
% -------------------------------------------------------------------------


%% SHALLOW SETUP
%   This section sets up the initial path, gets this filename, etc.

% Clean up environment
clear all;
close all;
clc;

% Who needs warnings
% warning off all
warning on all

% Name of this file
% localFilename = 'main.m';

% Build the path
% run('buildPath.m')
os_error_msg = 'Where''d you get this OS, the toilet store?';

% Construct path
if ispc
%     cd(fileparts(which(localFilename)));
    cd(fileparts(which(mfilename)));
    top_path = pwd;
elseif isunix
    localFolder = '~/MEDICOM/MATLAB/';
    old_path = cd(localFolder);
    top_path = pwd;
else
    error(os_error_msg)
end

% Add the paths
% addpath(genpath(top_path));
addpath(genpath('utilities'));
addpath(genpath('scripts'));
addpath(genpath('classes'));
addpath(genpath('surfmaps'));
