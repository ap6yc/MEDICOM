clear all
close all
clc

warning off all

% Set a random number generator seed
rng(100);

% SPICE function paths and data loading
% mice_path = '../../SPICE/mice/';
mice_path = '../../../SPICE/mice/';
kernels_path = '../../../kernels/';
try
    addpath([mice_path,'lib']);
    addpath([mice_path,'src/mice']);
    cspice_furnsh([kernels_path,'naif0012.tls.pc'])
    cspice_furnsh([kernels_path,'pck00010.tpc'])
    cspice_furnsh([kernels_path,'de430.bsp'])
%     addpath('/Users/demarsk/Documents/Research/Repository/MICE/mac64/lib');
%     addpath('/Users/demarsk/Documents/Research/Repository/MICE/mac64/src/mice');
%     cspice_furnsh('/Users/demarsk/Documents/Research/Repository/MICE/data/naif0010.tls.pc');
%     cspice_furnsh('/Users/demarsk/Documents/Research/Repository/MICE/data/pck00010.tpc');
%     cspice_furnsh('/Users/demarsk/Documents/Research/Repository/MICE/data/de430.bsp');
catch
%     addpath('/Users/demarsk/Documents/Research/SVN/MICE/mac64/lib');
%     addpath('/Users/demarsk/Documents/Research/SVN/MICE/mac64/src/mice');
%     cspice_furnsh('/Users/demarsk/Documents/Research/SVN/MICE/data/naif0010.tls.pc');
%     cspice_furnsh('/Users/demarsk/Documents/Research/SVN/MICE/data/pck00010.tpc');
%     cspice_furnsh('/Users/demarsk/Documents/Research/SVN/MICE/data/de430.bsp');
    error('You fool!')
end

% Constants
GM   = 3.986004415e5;      % [km^3/s^2]
req  = 6378.137;           % [km] WGS84 value
omg  = 7.2921151467064e-5; % [rad/s] IERS value

% Conversion constants
asc2deg = 1.0/3600.0;
deg2rad = pi/180.0;
asc2rad = asc2deg*deg2rad;
rad2asc = 3600.0*180.0/pi;
rad2deg = 180.0/pi;
dy2hr   = 24.0;
hr2mn   = 60.0;
mn2sc   = 60.0;
dy2sc   = 86400.0;
hr2sc   = 3600.0;

% TLE data retrieved from celestrak.net at 2000 on 10/03/17
% GPS BIIF-5  (PRN 30)    
TLE(1,:) = '1 39533U 14008A   17276.24151446  .00000017  00000-0  00000+0 0  9993';
TLE(2,:) = '2 39533  54.2433 279.4267 0030779 189.3241 170.6439  2.00569339 26473';

% Convert time of the TLE to ephemeris time
TLEdatestr = TLE(1,19:32);
TLEyr = strcat('20',TLEdatestr(1:2));
TLEdy = TLEdatestr(3:5);
TLEnm = str2double(TLEdatestr(6:end));
TLEnm = TLEnm*dy2hr;
TLEhr = floor(TLEnm);
TLEnm = TLEnm - TLEhr;
TLEnm = TLEnm*hr2mn;
TLEmn = floor(TLEnm);
TLEnm = TLEnm - TLEmn;
TLEsc = TLEnm*mn2sc;
TLEhr = int2str(TLEhr);
if(length(TLEhr)==1), TLEhr = ['0',TLEhr]; end
TLEmn = int2str(TLEmn);
if(length(TLEmn)==1), TLEmn = ['0',TLEmn]; end
TLEdoystr = [TLEyr,'-',TLEdy,'::',TLEhr,':',TLEmn,':',num2str(TLEsc)];
ET0 = cspice_str2et(TLEdoystr);

% Specify observer lat/lon (assumed to be on sph surf) -- MAUI GEODSS
phi  =   20.7088*deg2rad; % [deg] -> [rad]
lam  = -156.2578*deg2rad; % [deg] -> [rad]
slam = sin(lam);
clam = cos(lam);
sphi = sin(phi);
cphi = cos(phi);
% Compute observer position in the fixed frame
rsf  = req*[cphi*clam;cphi*slam;sphi];
% Compute the rotation matrix from the fixed frame to the surface frame
Tfs  = [-slam,clam,0.0;-sphi*clam,-sphi*slam,cphi;cphi*clam,cphi*slam,sphi];
% Specify elevation mask
mask = 20.0; % [deg]
% Measurement noise properties
Lk   = diag([3.0;3.0]); % [arcsec]
% Probabilities of failure
pfe  = 0.05; % extreme failure
pfm  = 0.15; % moderate failure

% check the time at the station
[~,~,~,time,~] = cspice_et2lst(ET0,399,lam,'PLANETOCENTRIC');

% Convert object from TLE -> KEP -> CAR
KEP = tle2kep(TLE,GM);
CAR = kep2car(KEP,GM,'rad');

% Specify a time interval for generating data
t0 =  0.0;
dt = 10.0;
tf = t0 + 10.0*dy2sc;
tv = t0:dt:tf;

% Propagate object using two-body equations of motion
opt     = odeset('AbsTol',1e-9,'RelTol',1e-9);
[TT,XX] = ode45(@eom_car,tv,CAR,opt,GM);
% Plot the orbit in the inertial frame
figure(1)
plot_3Dorbit(XX(:,1),XX(:,2),XX(:,3),req,'surfmaps/earth4.jpg');
view(177,34)
axis off
% print('GPSorbit','-dpng')

% Compute the lat/lon (for ground track) and elevation (for visibility) of the object
lat = zeros(length(TT),1);
lon = zeros(length(TT),1);
ele = zeros(length(TT),1);
tim = zeros(length(TT),1);
for i = 1:length(TT);
    % time
    ti     = TT(i);
    % rotation matrix from inertial frame to fixed frame
    Tif    = cspice_pxform('J2000','IAU_EARTH',ET0+ti);
    % position of object in inertial frame
    roi    = XX(i,1:3)';
    % position of object in fixed frame
    rof    = Tif*roi;
    % lat/lon caclulations
    lat(i) = atan2(rof(3),sqrt(rof(1)*rof(1)+rof(2)*rof(2)))*rad2deg;
    lon(i) = atan2(rof(2),rof(1))*rad2deg;
    % position of object wrt observer in surface frame
    ross   = Tfs*(Tif*roi - rsf);
    % elevation of object wrt the observer
    ele(i) = asin(ross(3)/norm(ross));
    % determine the time at the station
    [~,~,~,time,~] = cspice_et2lst(ET0+ti,399,lam,'PLANETOCENTRIC');
    % determine if it is night (past 10pm or before 4am)
    if(str2double(time(1:2)) >= 22.0 || str2double(time(1:2)) < 4.0)
        tim(i) = 1.0;
    end
end
% Plot the groundtrack
figure(2)
C = get(gca,'ColorOrder');
plot_groundtrack(lat,lon,'surfmaps/earth4.jpg');
hold on
plot(lam*rad2deg,phi*rad2deg,'p','MarkerSize',10,'MarkerEdgeColor',C(3,:),'MarkerFaceColor',C(3,:))
% print('GPSgroundtrack','-dpng')
% matlab2tikz('groundtrack.tex','height','\figureheight','width','\figurewidth','parseStrings',false);

% Plot the elevation
figure(3)
C = get(gca,'ColorOrder');
XL = [0,10];
YL = [-75,75];
idxs = find(tim == 0.0);
didx = find(diff(idxs) > 1);
ibeg = idxs(1);
for i = 1:length(didx)
    iend = idxs(didx(i));
    rectangle('Position',[TT(ibeg)./dy2sc,YL(1),(TT(iend)-TT(ibeg))./dy2sc,YL(2)-YL(1)],'FaceColor',[250 250 210]./255,'EdgeColor','none')
    ibeg = idxs(didx(i)+1);
end
iend = idxs(end);
rectangle('Position',[TT(ibeg)./dy2sc,YL(1),(TT(iend)-TT(ibeg))./dy2sc,YL(2)-YL(1)],'FaceColor',[250 250 210]./255,'EdgeColor','none')
rectangle('Position',[XL(1),YL(1),XL(2)-XL(1),mask-YL(1)],'FaceColor',[0.9 0.9 0.9],'EdgeColor','none')
hold on
plot(TT./dy2sc,ele*rad2deg,'Color',C(1,:),'LineWidth',1.2);
% plot(TT./dy2sc,45*tim,'Color',[0,0,0],'LineWidth',1.2);
box on
xlim([0,10])
ylim([-75,75])
% matlab2tikz('elevation.tex','height','\figureheight','width','\figurewidth','parseStrings',false);

% find the time indices where it is night at the station
idxs = find(tim > 0.0 & ele*rad2deg > mask);
didx = find(diff(idxs) > 1);
nigt = zeros(2,length(didx)+1);
nigt(1,1) = idxs(1);
nigt(2,1) = idxs(didx(1));
for i = 2:length(didx)
    nigt(1,i) = idxs(didx(i-1)+1);
    nigt(2,i) = idxs(didx(i));
end
nigt(1,i+1) = idxs(didx(i)+1);
nigt(2,i+1) = idxs(end);

% for each night, pick a random starting index
dur = zeros(size(nigt));
lav = 120;
ldl = 20;
for i = 1:size(nigt,2)
    len = randi([lav-ldl,lav+ldl]);
    dur(1,i) = randi([nigt(1,i) nigt(2,i)-len],1,1);
    dur(2,i) = dur(1,i) + len;
end

% determine the indices of the measurements
id = [];
for i = 1:size(dur,2)
    id = cat(1,id,(dur(1,i):dur(2,i))');
end

% Generate synthetic measurement data
ctr = 0;
Tm  = [];
Zt  = [];
Zm  = [];
Rm  = [];
Xt  = [];
Rt  = [];
EL  = [];
for i = 1:length(id);
    % time
    ti     = TT(id(i));
    % rotation matrix from inertial frame to fixed frame
    Tif    = cspice_pxform('J2000','IAU_EARTH',ET0+ti);
    % position of object in inertial frame
    roi    = XX(id(i),1:3)';
    % position of object wrt observer in surface frame
    ross   = Tfs*(Tif*roi - rsf);
    % elevation of object wrt the observer
    El     = asin(ross(3)/norm(ross));
    % if the object is above the elevation mask, continue
    if(El*rad2deg > mask)
        % position of the observer in the inertial frame
        rsi  = (Tif')*rsf;
        % position of the object wrt the observer in the inertial frame
        rosi = roi - rsi;
        % right-ascension (true)
        ra   = atan2(rosi(2),rosi(1));
        % declination (true)
        dc   = atan2(rosi(3),sqrt(rosi(1)*rosi(1)+rosi(2)*rosi(2)));
        % total measurement
        zi = [ra;dc]*rad2asc + Lk*randn(2,1);
        % store the data
        ctr = ctr + 1;
        Tm  = cat(1,Tm,ti);             % measurement time [sec]
        Zt  = cat(2,Zt,[ra;dc]);        % true measurement [rad]
        Zm  = cat(2,Zm,zi);             % measurement [arcsec]
        Rm  = cat(3,Rm,Lk*Lk');         % measurement noise covariance [arcsec^2]
        Xt  = cat(2,Xt,XX(id(i),1:6)'); % true state (pos/vel) [km and km/s]
        Rt  = cat(2,Rt,rsi);            % true inertial observer position [km]
        EL  = cat(1,EL,El);
    else
        disp(El*rad2deg)
    end
end

c1 = 0;
c2 = 0;
c3 = 0;
for i = 1:length(id);
    % true measurement
    zi = Zt(:,i);
    % probability of failure
    fail = rand;
    % total measurement
    if(fail < pfe && i > 241)
        % extreme measurement noise
        zi = zi*rad2asc + 100.0*Lk*randn(2,1);
        c1 = c1 + 1;
    elseif(fail < (pfe + pfm) && i > 241)
        % moderate measurement noise
        zi = zi*rad2asc + 10.0*Lk*randn(2,1);
        c2 = c2 + 1;
    else
        % nominal measurement noise
        zi = Zm(:,i);
        c3 = c3 + 1;
    end
    Zm(:,i) = zi;
end

figure(3)
[ax1] = subplot(3,1,1);
plot(Tm./hr2sc,Zm(1,:)*asc2deg,'.')
hold on
ylabel('RA [deg]')
[ax2] = subplot(3,1,2);
plot(Tm./hr2sc,Zm(2,:)*asc2deg,'.')
hold on
ylabel('DEC [deg]')
[ax3] = subplot(3,1,3);
plot(Tm./hr2sc,EL*rad2deg,'.')
hold on
ylabel('EL [deg]')
xlabel('Time [hr]')
linkaxes([ax1,ax2,ax3],'x')
% matlab2tikz('measurements.tex','height','\figureheight','width','\figurewidth','parseStrings',false);

% DATA PROVIDED ARE:
%   Tm = (1 x n) array of observation times [sec]
%   Zm = (2 x n) array of right-ascension and declination observations [arcsec]
%   Rm = (2 x 2 x n) array of measurement noise covariances [arcsec^2]
%   Xt = (6 x n) array of true object position and velocity [km] and [km/s]
%   Rt = (3 x n) array of observer position in inertial frame [km]
save('data_HW07','Tm','Zm','Rm','Xt','Rt')