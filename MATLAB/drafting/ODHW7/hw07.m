clear all
close all
clc

% Constants
GM   = 3.986004415e5;      % [km^3/s^2]
req  = 6378.137;           % [km] WGS84 value
omg  = 7.2921151467064e-5; % [rad/s] IERS value

% Conversion constants
asc2deg = 1.0/3600.0;
deg2rad = pi/180.0;
asc2rad = asc2deg*deg2rad;
rad2asc = 3600.0*180.0/pi;
rad2deg = 180.0/pi;
dy2hr   = 24.0;
hr2mn   = 60.0;
mn2sc   = 60.0;
dy2sc   = 86400.0;
hr2sc   = 3600.0;

% DATA PROVIDED ARE:
%   Tm = (1 x n) array of observation times [sec]
%   Zm = (2 x n) array of right-ascension and declination observations [arcsec]
%   Rm = (2 x 2 x n) array of measurement noise covariances [arcsec^2]
%   Xt = (6 x n) array of true object position and velocity [km] and [km/s]
%   Rt = (3 x n) array of observer position in inertial frame [km]
load('data_HW07','Tm','Zm','Rm','Xt','Rt')

% -------------------------------------------------------------------------
%%% Problem 1
disp('Problem #1')

% Perform initial orbit determination via Gauss' method with Gibbs' method
% extract three measurements (time, RA, DEC, inertial position of station)
% we're using data points 1, 61, and 122 for IOD
iIOD = [1;61;122];
tIOD = Tm(iIOD);
aIOD = Zm(1,iIOD)*asc2deg; % [arcsec] -> [deg]
dIOD = Zm(2,iIOD)*asc2deg; % [arcsec] -> [deg]
rIOD = Rt(:,iIOD);

% apply Gauss' method with Gibbs' method,,,
[t2,x2] = IODgauss(tIOD,aIOD,dIOD,rIOD,GM,'Gibbs');

% compute pos/vel err at t2
rerr_gauss_t2 = norm(x2(1:3) - Xt(1:3,iIOD(2)));
verr_gauss_t2 = 1e3*norm(x2(4:6) - Xt(4:6,iIOD(2)));
disp('   IOD at t2')
disp(['      pos error = ',num2str(rerr_gauss_t2,'%-.3f'),' km'])
disp(['      vel error = ',num2str(verr_gauss_t2,'%-.3f'),' m/s'])
% -------------------------------------------------------------------------



% -------------------------------------------------------------------------
%%% Problem 2
disp(' ')
disp('Problem #2')

% propagate Gauss solution at t2 back to t1
opt    = odeset('AbsTol',1e-9,'RelTol',1e-9);
[~,XX] = ode45(@eom_car,[t2,tIOD(1)],x2,opt,GM);
t1     = tIOD(1);
x1     = XX(end,:)';

% Perform iterative improvement via batch least squares (LUMVE)
% set the number of measurements to use
% we're using all of the data in the first two arcs
kmax  = 241;

% set the epoch time, reference state, initial deviation, and covariance
t0    = t1;
x0ref = x1;
x0    = zeros(6,1);

% iterate until the pos changes are < 100 m and the vel changes are < 100 mm/s
pbnd = 0.1;
vbnd = 1e-4;
imax = 4;

% begin the iteration loop
exit = false;
iter = 0;
while(~exit)
    % update iteration counter
    iter = iter + 1;
    
    % no prior information, so Lambda and lambda are both zero
    Lam  = zeros(6,6);
    lam  = zeros(6,1);
    
    % initialize the reference state and the STM
    xref = x0ref;
    Phi  = eye(6);
    
    % propagate the reference state and STM across the entire timespan
    opts   = odeset('AbsTol',1e-9,'RelTol',1e-9);
    [~,XX] = ode45(@eom_tbp_ref,Tm(1:kmax),[xref;Phi(:)],opts,GM);
        
    % declare storage for the time, index, and residual
    rest = zeros(kmax,1);
    resi = zeros(kmax,1);
    resm = zeros(kmax,2);
    for k = 1:kmax
        % extract the time, measurement, covariance, and station position for the kth observation
        tk = Tm(k);
        zk = Zm(:,k);
        Rk = Rm(:,:,k);
        rk = Rt(:,k);
        
        % determine the LUMVE weighting matrix
        Ri = inv(Rk);
        
        % unpack the reference state and STM
        xref   = XX(k,1:6)';
        Phi    = reshape(XX(k,7:end)',6,6);
        
        % compute the reference state measurement (RA and DEC)
        %   form the relative position, then compute the ref. measurement
        %   change the units of the reference measurement to [arcsec]
        rosi  = xref(1:3) - rk;
        x     = rosi(1);
        y     = rosi(2);
        z     = rosi(3);
        wsq   = x*x + y*y;
        w     = sqrt(wsq);
        rhosq = wsq + z*z;
        zref  = [atan2(y,x);atan2(z,w)];
        zref  = zref*rad2asc;
        
        % compute the measurement mapping matrix (Htilde)
        %   change the units of the Jacobian to [arcsec]
        Ht = [        -y/wsq,          x/wsq,     0.0, 0.0, 0.0, 0.0;
              -x*z/(w*rhosq), -y*z/(w*rhosq), w/rhosq, 0.0, 0.0, 0.0];
        Ht = Ht*rad2asc;
        
        % time-mapping of the observation matrix
        H = Ht*Phi;
        
        % accumulate the lambdas for LUMVE
        lam = lam + H'*Ri*(zk - zref);
        Lam = Lam + H'*Ri*H;
        
        % store the time, index, and residual for plotting later
        rest(k)   = tk;
        resi(k)   = k;
        resm(k,:) = zk - zref;
    end
    % get the least squares solution
    delx = Lam\lam;
    
    % perform the iteration by shifting the reference and the estimated deviation
    x0ref = x0ref + delx;
    x0    = x0 - delx;
    
    % plot the measurement residuals for right-ascension on subplots
    figure(1)
    C = get(gca,'ColorOrder');
    subplot(imax,1,iter)
    plot(resi,resm(:,1),'x','Color',C(iter,:),'LineWidth',1.2,'MarkerSize',5)
    ylabel(['Iter \#',int2str(iter)])
    
    % plot the measurement residuals for declination on subplots
    figure(2)
    C = get(gca,'ColorOrder');
    subplot(imax,1,iter)
    plot(resi,resm(:,2),'x','Color',C(iter,:),'LineWidth',1.2,'MarkerSize',5)
    ylabel(['Iter \#',int2str(iter)])
    
    % compute position/velocity deltas
    pdelt = norm(delx(1:3));
    vdelt = norm(delx(4:6));
    
    % check for convergence
    if(pdelt <= pbnd && vdelt <= vbnd)
        exit = true;
        break
    end
end
disp(['   Batch least-squares converged in ',int2str(iter),' iterations'])

figure(1)
xlabel('Measurement Index')
subplot(imax,1,1)
title('Right-Ascension Residuals at Each Iteration [arcsec]')

figure(2)
xlabel('Measurement Index')
subplot(imax,1,1)
title('Declination Residuals at Each Iteration [arcsec]')

% determine the estimated state and covariance
xhat = x0ref;
P    = inv(Lam);

% compute pos/vel err at t1
rerr_LSQ_t1 = 1e3*norm(xhat(1:3) - Xt(1:3,iIOD(1)));
verr_LSQ_t1 = 1e6*norm(xhat(4:6) - Xt(4:6,iIOD(1)));
disp('   Batch least-squares at t1')
disp(['      pos error = ',num2str(rerr_LSQ_t1,'%-.3f'),' m'])
disp(['      vel error = ',num2str(verr_LSQ_t1,'%-.3f'),' mm/s'])
% compute pos/vel std at t1
rsig_LSQ_t1 = 1e3*sqrt(diag(P(1:3,1:3)));
vsig_LSQ_t1 = 1e6*sqrt(diag(P(4:6,4:6)));
disp(['      pos x sigma = ',num2str(rsig_LSQ_t1(1),'%-.3f'),' m'])
disp(['      pos y sigma = ',num2str(rsig_LSQ_t1(2),'%-.3f'),' m'])
disp(['      pos z sigma = ',num2str(rsig_LSQ_t1(3),'%-.3f'),' m'])
disp(['      vel x sigma = ',num2str(vsig_LSQ_t1(1),'%-.3f'),' mm/s'])
disp(['      vel y sigma = ',num2str(vsig_LSQ_t1(2),'%-.3f'),' mm/s'])
disp(['      vel z sigma = ',num2str(vsig_LSQ_t1(3),'%-.3f'),' mm/s'])
% -------------------------------------------------------------------------



% -------------------------------------------------------------------------
%%% Problem 3/4
disp(' ')
disp('Problem #3/4')
which = 4;

% determine the number of data points to process with the EKF
nelm = length(kmax+1:length(Tm));

% declare storage space for saving residual information
%    kres = measurement index of the residual
%    resi = residual
%    wstd = standard deviation of the residual covariance
%    rstd = standard deviation of the measurement noise covariance
kres = zeros(1,nelm);
dres = zeros(1,nelm);
eres = zeros(1,nelm);
resi = zeros(2,nelm);
wstd = zeros(2,nelm);
rstd = zeros(2,nelm);

% declare storage space for saving state estimation error information
%    kerr = measurement index of the state estimation error
%    xerr = state estimation error
%    xstd = standard deviation of the state estimation error covariance
kerr = zeros(1,2*nelm);
xerr = zeros(6,2*nelm);
xstd = zeros(6,2*nelm);

% initialize counters for saving state/residual information 
xctr = 0;
rctr = 0;

% define the process noise mapping matrix and the process noise PSD
M = [zeros(3,3);eye(3)];
Q = (0.0)^2*eye(3);

% residual editing parameter
switch(which)
    case 3
        gamma = inf;
    case 4
        gamma = 15.20;
end

% initialize time, mean, and covariance for the EKF
tkm1 = t1;
mkm1 = xhat;
Pkm1 = P;
for k = kmax+1:length(Tm)
    % unpack the time, meas, meas noise cov, and station position
    tk = Tm(k);
    zk = Zm(:,k);
    Rk = Rm(:,:,k);
    rk = Rt(:,k);
    
    % propagate the mean and covariance
    opts   = odeset('AbsTol',1e-9,'RelTol',1e-9);
    [~,XX] = ode45(@eom_tbp_ekf,[tkm1,tk],[mkm1;Pkm1(:)],opts,GM,M,Q);
    mkm    = XX(end,1:6)';
    Pkm    = reshape(XX(end,7:end)',6,6);
    Pkm    = 0.5*(Pkm + Pkm');
    
    % compute the estimated measurement
    %   form the relative position, then compute the expected measurement
    %   change the units of the reference measurement to [arcsec]
    rosi  = mkm(1:3) - rk;
    x     = rosi(1);
    y     = rosi(2);
    z     = rosi(3);
    wsq   = x*x + y*y;
    w     = sqrt(wsq);
    rhosq = wsq + z*z;
    zhat  = [atan2(y,x);atan2(z,w)];
    zhat  = zhat*rad2asc;
    
    % compute the measurement Jacobian
    %   change the units of the Jacobian to [arcsec]
    Hk = [        -y/wsq,          x/wsq,     0.0, 0.0, 0.0, 0.0;
          -x*z/(w*rhosq), -y*z/(w*rhosq), w/rhosq, 0.0, 0.0, 0.0];
    Hk = Hk*rad2asc;
    
    % update the mean and covariance
    Ck  = Pkm*Hk';
    Wk  = Hk*Pkm*Hk' + Rk;
    
    % Mahalanobis distance
    d = (zk - zhat)'*(Wk\(zk - zhat));
    if(d < gamma)
        % Kalman gain
        Kk = Ck/Wk;
        
        % mean and covariance updates
        mkp = mkm + Kk*(zk - zhat);
        Pkp = Pkm - Kk*Hk*Pkm;
    else
        % measurement is not processed
        mkp = mkm;
        Pkp = Pkm;
    end
    Pkp = 0.5*(Pkp + Pkp');
    
    % store the prior index, error, and standard deviations
    xctr         = xctr + 1;
    kerr(:,xctr) = k;
    xerr(:,xctr) = Xt(:,k) - mkm;
    xstd(:,xctr) = sqrt(diag(Pkm));
    
    % store the posterior index, error, and standard deviations
    xctr         = xctr + 1;
    kerr(:,xctr) = k;
    xerr(:,xctr) = Xt(:,k) - mkp;
    xstd(:,xctr) = sqrt(diag(Pkp));
    
    % store the measurement index, residual, and standard deviations of Wk and Rk
    rctr         = rctr + 1;
    kres(:,rctr) = k;
    dres(:,rctr) = d;
    eres(:,rctr) = (d < gamma);
    resi(:,rctr) = zk - zhat;
    wstd(:,rctr) = sqrt(diag(Wk));
    rstd(:,rctr) = sqrt(diag(Rk));
    
    % cycle the time, mean, and covariance for the next step of the EKF
    tkm1 = tk;
    mkm1 = mkp;
    Pkm1 = Pkp;
end
% compute pos/vel sigmas at the final time
disp('   EKF at tf')
rsig_EKF_tf = 1e3*sqrt(diag(Pkp(1:3,1:3)));
vsig_EKF_tf = 1e6*sqrt(diag(Pkp(4:6,4:6)));
disp(['      pos x sigma = ',num2str(rsig_EKF_tf(1),'%-.3f'),' m'])
disp(['      pos y sigma = ',num2str(rsig_EKF_tf(2),'%-.3f'),' m'])
disp(['      pos z sigma = ',num2str(rsig_EKF_tf(3),'%-.3f'),' m'])
disp(['      vel x sigma = ',num2str(vsig_EKF_tf(1),'%-.3f'),' mm/s'])
disp(['      vel y sigma = ',num2str(vsig_EKF_tf(2),'%-.3f'),' mm/s'])
disp(['      vel z sigma = ',num2str(vsig_EKF_tf(3),'%-.3f'),' mm/s'])

% plot the measurement residuals for right-ascension and declination
% include the 3sigma of the residual covariance and measurement noise covariance
figure(3)
C = get(gca,'ColorOrder');
off = 0;
for idx = 1:2
    subplot(2,1,idx)
    plot(kres(eres==1),resi(idx+off,eres==1),'x','Color',C(1,:))
    hold on
    plot(kres(eres==0),resi(idx+off,eres==0),'x','Color',C(7,:))
    plot(kres,+3.0*rstd(idx+off,:),'Color',[0,0,0],'LineWidth',1.2)
    plot(kres,-3.0*rstd(idx+off,:),'Color',[0,0,0],'LineWidth',1.2)
    plot(kres,+3.0*wstd(idx+off,:),'Color', C(2,:),'LineWidth',1.2)
    plot(kres,-3.0*wstd(idx+off,:),'Color', C(2,:),'LineWidth',1.2)
    ylim([-50,50])
end
subplot(2,1,1)
ylabel('Right-ascension Residual [arcsec]')
subplot(2,1,2)
ylabel('Declination Residual [arcsec]')
xlabel('Measurement Index')
% matlab2tikz(['hw07_prob0',int2str(which),'_resid.tex'],'height','\figureheight','width','\figurewidth','parseStrings',false)

figure(4)
C = get(gca,'ColorOrder');
plot(kres(eres==1),dres(eres==1),'x','Color',C(1,:))
hold on
plot(kres(eres==0),dres(eres==0),'x','Color',C(7,:))
plot(get(gca,'XLim'),[gamma,gamma],'Color',[0,0,0],'LineWidth',1.2)
set(gca,'YScale','log')
ylabel('Squared Mahalanobis Distance')
xlabel('Measurement Index')
% matlab2tikz(['hw07_prob0',int2str(which),'_edit.tex'],'height','\figureheight','width','\figurewidth','parseStrings',false)

% plot the state estimation error for position
% include the 3sigma of the position estimation error covariance
figure(5)
C = get(gca,'ColorOrder');
off = 0;
for idx = 1:3
    subplot(3,1,idx)
    plot(kerr,     1e3*xerr(idx+off,:),'Color',C(1,:),'LineWidth',1.0)
    hold on
    plot(kerr,+3.0*1e3*xstd(idx+off,:),'Color',C(2,:),'LineWidth',1.2)
    plot(kerr,-3.0*1e3*xstd(idx+off,:),'Color',C(2,:),'LineWidth',1.2)
    ylim([-500,500])
end
subplot(3,1,2)
ylabel('Position Error [m]')
subplot(3,1,3)
xlabel('Measurement Index')
% matlab2tikz(['hw07_prob0',int2str(which),'_poserr.tex'],'height','\figureheight','width','\figurewidth','parseStrings',false)


% plot the state estimation error for velocity
% include the 3sigma of the velocity estimation error covariance
figure(6)
C = get(gca,'ColorOrder');
off = 3;
for idx = 1:3
    subplot(3,1,idx)
    plot(kerr,     1e6*xerr(idx+off,:),'Color',C(1,:),'LineWidth',1.0)
    hold on
    plot(kerr,+3.0*1e6*xstd(idx+off,:),'Color',C(2,:),'LineWidth',1.2)
    plot(kerr,-3.0*1e6*xstd(idx+off,:),'Color',C(2,:),'LineWidth',1.2)
    ylim([-100,100])
end
subplot(3,1,2)
ylabel('Velocity Error [mm/s]')
subplot(3,1,3)
xlabel('Measurement Index')
% matlab2tikz(['hw07_prob0',int2str(which),'_velerr.tex'],'height','\figureheight','width','\figurewidth','parseStrings',false)

