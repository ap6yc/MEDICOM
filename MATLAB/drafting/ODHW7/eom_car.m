% function [dzdt] = eom_iod_ukf(ET,z,n,k,GM)
% 
% S = reshape(z,n,k);
% F = zeros(n,k);
% for i = 1:k
%     ri = [S(1,i);S(2,i);S(3,i)];
%     vi = [S(4,i);S(5,i);S(6,i)];
%     
%     rm = norm(ri);
%     r2 = rm*rm;
%     ai = -(GM/r2)*(ri/rm);
%     
%     F(1,i) = vi(1);
%     F(2,i) = vi(2);
%     F(3,i) = vi(3);
%     F(4,i) = ai(1);
%     F(5,i) = ai(2);
%     F(6,i) = ai(3);
% end
% dzdt = reshape(F,n*k,1);
% 
% end

function [dzdt] = eom_car(ET,z,GM)

rx = z(1);
ry = z(2);
rz = z(3);

rm = sqrt(rx*rx + ry*ry + rz*rz);
r2 = rm*rm;

ax = -(GM/r2)*(rx/rm);
ay = -(GM/r2)*(ry/rm);
az = -(GM/r2)*(rz/rm);

dzdt = [z(4);z(5);z(6);ax;ay;az];

end