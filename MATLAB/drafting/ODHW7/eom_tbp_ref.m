function [dqdt] = eom_tbp_ref(~,q,GM)

% Name: eom_tbp_ref
% Author: Kyle J. DeMars

% Extract state elements from q
x  = q(1);
y  = q(2);
z  = q(3);
xd = q(4);
yd = q(5);
zd = q(6);
Ph = reshape(q(7:42),6,6);

r2 = x*x + y*y + z*z;
r  = sqrt(r2);

s = x/r;
t = y/r;
u = z/r;

f1 = GM/r2;
f2 = f1/r;

% Compute derivatives of the potential function (the acceleration)
Ux = -f1*s;
Uy = -f1*t;
Uz = -f1*u;

Gxx = f2*(3.0*s*s - 1.0);
Gxy = f2*(3.0*s*t);
Gxz = f2*(3.0*s*u);
Gyy = f2*(3.0*t*t - 1.0);
Gyz = f2*(3.0*t*u);
Gzz = f2*(3.0*u*u - 1.0);

f = [xd;yd;zd;Ux;Uy;Uz];
F = [0.0,0.0,0.0,1.0,0.0,0.0;...
     0.0,0.0,0.0,0.0,1.0,0.0;...
     0.0,0.0,0.0,0.0,0.0,1.0;...
     Gxx,Gxy,Gxz,0.0,0.0,0.0;...
     Gxy,Gyy,Gyz,0.0,0.0,0.0;...
     Gxz,Gyz,Gzz,0.0,0.0,0.0];
A = F*Ph;
 

% Assemble time derivatives for output
dqdt = [f;reshape(A,36,1)];

return
end