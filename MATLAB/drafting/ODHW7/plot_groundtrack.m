function h = plot_groundtrack(lat,lon,varargin)

% REQUIRED
%   lat = latitude,  n x 1 array, in degrees
%   lon = longitude, n x 1 array, in degrees
% OPTIONAL
%   fname = varargin{1} = filename with path, string array
% CHANGELOG
% -- 09/15/15 -- KJD
%    initial creation
% -- 09/23/15 -- KJD
%    added off-axis plotting so that lines are cut off at edges
%    fixed a bug where the y-axis was inverted
% -- 10/08/15 -- KJD
%    changed flipud to flipdim for backwards support
%    changed axis labeling method for backwards support
% -- 10/15/15 -- KJD
%    added check for the case that there are no wraparounds in longitude
% NOTES
% -- use verLessThan to act differently in different versions


% read in an image
%   - can be user-specified in the variable arguments
%   - if not specified, a default image is used
nvarargin = length(varargin);
if(nvarargin > 0)
    fname = varargin{1};
else
    fname = 'surfmaps/earth1.jpg';
end
img = imread(fname);
% flip the image upside down
img = flipdim(img,1);

% plot the image on a set of axes
%   - set the limits to be -180 to 180 and -90 to 90 in x and y
%   - set the axes to be equal so that proportions are correct
%   - set the tick labels for consistency
h = imagesc([-180 180],[-90 90],img);
set(gca,'YDir','normal')
axis equal
xlim([-180,180])
ylim([-90,90])
% ax = gca;
% ax.XTick = -180:60:180;
% ax.YTick = -90:30:90;
set(gca,'XTick',-180:60:180);
set(gca,'YTick',-90:30:90);
hold on
C = get(gca,'ColorOrder');
    
% plot the lat/lon data
%   - check for wrap arounds and plot each consecutive set as one line
%   - then, plot the last segment if there is one
%   - if there are no wrap arounds, idxs is empty
idxs = find(abs(diff(lon))>180.0);
switch isempty(idxs)
    case true
        plot(lon,lat,'Color',C(2,:),'LineWidth',1.3)
    otherwise
        idx0 = 1;
        for i = 1:length(idxs)
            % extract a single set of lat/lon
            latidx = lat(idx0:idxs(i));
            lonidx = lon(idx0:idxs(i));
            % tack on one more point to the left
            if(idx0 > 1)
                latidx = [lat(idx0-1);      latidx];
                lonidx = [lon(idx0-1)-360.0;lonidx];
            end
            % and tack on one more point to the right
            if(idxs(i) < length(lon))
                latidx = [latidx;lat(idxs(i)+1)      ];
                lonidx = [lonidx;lon(idxs(i)+1)+360.0];
            end
            % those get plotted off screen, but it makes it look more continuous
            % across the earth
            plot(lonidx,latidx,'Color',C(2,:),'LineWidth',1.3)
            idx0 = idxs(i)+1;
        end
        if(idxs(end) < length(lon))
            plot(lon(idx0:end),lat(idx0:end),'Color',C(2,:),'LineWidth',1.3)
        end
end

% label the axes
xlabel('Longitude [deg]')
ylabel('Latitude [deg]')