function [h] = plot_3Dorbit(x,y,z,R,varargin)

% REQUIRED
%   x = x-coordinate of position,  n x 1 array, in length units
%   y = y-coordinate of position,  n x 1 array, in length units
%   z = z-coordinate of position,  n x 1 array, in length units
%   R = radius of the body, 1 x 1, in length units
% OPTIONAL
%   fname = varargin{1} = filename with path, string array
% CHANGELOG
% -- 09/22/15 -- KJD
%    initial creation
% NOTES
% -- 


% read in an image
%   - can be user-specified in the variable arguments
%   - if not specified, a default image is used
nvarargin = length(varargin);
if(nvarargin > 0)
    fname = varargin{1};
else
    fname = 'surfmaps/earth1.jpg';
end
[C,M] = imread(fname);

% generate and scale a sphere to plot on
n       = 50;
[X,Y,Z] = sphere(n-1);
X       =  R*X;
Y       =  R*Y;
Z       = -R*Z;

% plot the central body
h = surface(X,Y,Z,C,'EdgeColor','none','FaceColor','texturemap');
if(min(size(M)) > 0), colormap(M); end;
axis equal;
view(3);

% add a plot of the orbit
hold on
C = get(gca,'ColorOrder');
plot3(x,y,z,'Color',C(2,:),'LineWidth',2)
xlabel('x [km]')
ylabel('y [km]')
zlabel('z [km]')
