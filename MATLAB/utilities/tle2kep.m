function [kep] = tle2kep(tle,mu)

deg2rad       = pi/180.0;
day2sec       = 86400.0;
rev2rad       = 2.0*pi;
revday2radsec = rev2rad/day2sec;

kep    = zeros(6,1);
kep(3) = str2double(tle(2,09:16))*deg2rad;
kep(4) = str2double(tle(2,18:25))*deg2rad;
kep(2) = str2double(['.',tle(2,27:33)]);
kep(5) = str2double(tle(2,35:42))*deg2rad;
manm   = str2double(tle(2,44:51))*deg2rad;
mmot   = str2double(tle(2,53:63))*revday2radsec;
kep(6) = manm;
kep(1) = (mu/mmot/mmot)^(1/3);

end
