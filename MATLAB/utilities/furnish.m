function furnish(opts, logger)
%FURNISH Furnish the environment with CSPICE kernesls
%   INPUTS:
%       opts
%   OUTPUTS:
%       N/A

    % Load the kernels
    logger.info(mfilename, 'Furnishing kernels');
    try
        cspice_furnsh([opts.kernels_path,'naif0012.tls.pc'])
        cspice_furnsh([opts.kernels_path,'pck00010.tpc'])
        cspice_furnsh([opts.kernels_path,'de430.bsp'])
    catch
        message = 'Missing kernels directory';
        logger.error(mfilename, message);
        error(message)
    end

end

