function [t2,x2] = IODgauss(t,a,d,r,GM,vmeth)

t1 = t(1); t2 = t(2); t3 = t(3);
a1 = a(1); a2 = a(2); a3 = a(3);
d1 = d(1); d2 = d(2); d3 = d(3);

robs1 = r(:,1);
robs2 = r(:,2);
robs3 = r(:,3);

u1 = [cosd(d1)*cosd(a1);cosd(d1)*sind(a1);sind(d1)];
u2 = [cosd(d2)*cosd(a2);cosd(d2)*sind(a2);sind(d2)];
u3 = [cosd(d3)*cosd(a3);cosd(d3)*sind(a3);sind(d3)];

tau1  = t1 - t2;
tau3  = t3 - t2;
tau13 = tau3 - tau1;

p1 = cross(u2,u3);
p2 = cross(u1,u3);
p3 = cross(u1,u2);

D0  = dot(u1,p1);
D11 = dot(robs1,p1);
D21 = dot(robs2,p1);
D31 = dot(robs3,p1);
D12 = dot(robs1,p2);
D22 = dot(robs2,p2);
D32 = dot(robs3,p2);
D13 = dot(robs1,p3);
D23 = dot(robs2,p3);
D33 = dot(robs3,p3);

A = (-D12*(tau3/tau13) + D22 + D32*(tau1/tau13))/D0;
B = (-D12*(tau13^2 - tau3^2)*(tau3/tau13) + D32*(tau13^2 - tau1^2)*(tau1/tau13))/(6.0*D0);

a = -A^2 - 2.0*A*dot(robs2,u2) - dot(robs2,robs2);
b = -2.0*GM*B*(A + dot(robs2,u2));
c = -GM^2*B^2;

p = roots([1.0,0.0,a,0.0,0.0,b,0.0,0.0,c]);

x2 = [];
for idx = 1:8
    if(imag(p(idx)) == 0.0 && real(p(idx)) > 0.0)
        % a real, positive solution
        sol = p(idx);
        
        %
        rho1 = ((6.0*(D31*(tau1/tau3) + D21*(tau13/tau3))*sol^3 + GM*D31*(tau13^2 - tau1^2)*(tau1/tau3))/(6.0*sol^3 + GM*(tau13^2 - tau3^2)) - D11)/D0;
        rho2 = A + GM*B*sol^(-3);
        rho3 = ((6.0*(D13*(tau3/tau1) - D23*(tau13/tau1))*sol^3 + GM*D13*(tau13^2 - tau3^2)*(tau3/tau1))/(6.0*sol^3 + GM*(tau13^2 - tau1^2)) - D33)/D0;
        
        %
        r1 = robs1 + rho1*u1;
        r2 = robs2 + rho2*u2;
        r3 = robs3 + rho3*u3;
        
        switch vmeth
            case 'fandg'
                %
                f1 = 1 - 0.5*GM*tau1^2/sol^3;
                f3 = 1 - 0.5*GM*tau3^2/sol^3;
                g1 = tau1 - (1.0/6.0)*GM*tau1^3/sol^3;
                g3 = tau3 - (1.0/6.0)*GM*tau3^3/sol^3;
                
                %
                v2 = (f1*r3 - f3*r1)/(f1*g3 - f3*g1);
            case 'Gibbs'
                r1m = norm(r1);
                r2m = norm(r2);
                r3m = norm(r3);
                
                r1xr2 = cross(r1,r2);
                r2xr3 = cross(r2,r3);
                r3xr1 = cross(r3,r1);
                
                n = r1m*r2xr3 + r2m*r3xr1+ r3m*r1xr2;
                d = r1xr2 + r2xr3 + r3xr1;
                s = (r2m - r3m)*r1 + (r3m - r1m)*r2 + (r1m - r2m)*r3;
                
                v2 = sqrt(GM/(norm(n)*norm(d)))*(cross(d,r2)/r2m + s);
            otherwise
                error('Unsupported velocity determination method')
        end     
        x2 = [x2,[r2;v2]];
    end
end