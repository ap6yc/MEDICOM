function [poolobj,poolsize] = initPool(run_parallel)
%INITPOOL - Initializes a parallel pool for the simulation
%   INPUTS:
%       run_parallel - flag to run in parallel or not
%   OUTPUTS:
%       poolobj      - handle of pool object
%       poolsize     - number of workers in the pool

    % Check if the machine has the parallel toolbox license
    has_parallel = license('test', 'parallel_toolbox') || ...
        license('test', 'Distrib_Computing_Toolbox');
    
    % Create/get parallel pool if running in parallel
    if run_parallel && has_parallel
        % Start Parallel Pool
        poolobj = gcp('nocreate'); % If no pool, do not create new one.
        if isempty(poolobj)
            poolobj = parpool;
            poolsize = 64;
        else
            poolsize = poolobj.NumWorkers;
        end
    else
        poolobj = NaN;
        poolsize = NaN;
    end
end