function [t2,x2] = IODlaplace(t,a,d,r,GM,omg,uvals)

t1 = t(1); t2 = t(2); t3 = t(3);
a1 = a(1); a2 = a(2); a3 = a(3);
d1 = d(1); d2 = d(2); d3 = d(3);

robs1 = r(:,1);
robs2 = r(:,2);
robs3 = r(:,3);

u1 = [cosd(d1)*cosd(a1);cosd(d1)*sind(a1);sind(d1)];
u2 = [cosd(d2)*cosd(a2);cosd(d2)*sind(a2);sind(d2)];
u3 = [cosd(d3)*cosd(a3);cosd(d3)*sind(a3);sind(d3)];

% observer position, velocity, and acceleration
r   = robs2;
rd  = cross([0.0;0.0;omg],r);
rdd = cross([0.0;0.0;omg],cross([0.0;0.0;omg],r));

if(isempty(uvals))
    % Lagrange interpolation coefficients
    s1  = (t2 - t3)/((t1 - t2)*(t1 - t3));
    s2  = (2.0*t2 - t1 - t3)/((t2 - t1)*(t2 - t3));
    s3  = (t2 - t1)/((t3 - t1)*(t3 - t2));
    s4  = 2.0/((t1 - t2)*(t1 - t3));
    s5  = 2.0/((t2 - t1)*(t2 - t3));
    s6  = 2.0/((t3 - t1)*(t3 - t2));
    % line of sight and interpolated rates
    u   = u2;
    ud  = s1*u1 + s2*u2 + s3*u3;
    udd = s4*u1 + s5*u2 + s6*u3;
else
    u   = uvals(:,1);
    ud  = uvals(:,2);
    udd = uvals(:,3);
end

% determinants
D0  = 2.0*det([u,ud,udd]);
D1  = det([u,ud,rdd]);
D2  = det([u,ud,r]);
D3  = det([u,rdd,udd]);
D4  = det([u,r,udd]);
% polynomial coefficients
a   = -4.0*(D1/D0)^2 + 4.0*(D1/D0)*dot(u,r) - dot(r,r);
b   = -8.0*GM*(D1/D0)*(D2/D0) + 4.0*GM*(D2/D0)*dot(u,r);
c   = -4.0*GM*GM*(D2/D0)^2;
% root calculations
rts = roots([1.0,0.0,a,0.0,0.0,b,0.0,0.0,c]);
% we continue only for the real, positive root(s)
x2  = [];
for idx = 1:8
    if(imag(rts(idx)) == 0.0 && real(rts(idx)) > 0.0)
        % a real, positive solution
        sol = rts(idx);
        
        % range and range-rate
        rho  = -2.0*(D1/D0) - 2.0*GM*sol^(-3)*(D2/D0);
        rhod = -(D3/D0) - GM*sol^(-3)*(D4/D0);
        
        % position and velocity
        rsol = r + rho*u;
        vsol = rd + rhod*u + rho*ud;
        
        x2 = [x2,[rsol;vsol]];
    end
end