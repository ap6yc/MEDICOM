function micePath(opts, logger)
%MICEPATH Adds the mice paths
%   INPUTS:
%       opts
%       logger
%   OUTPUTS:
%       N/A

    % Add the mice paths
    logger.debug(mfilename, 'Adding mice paths');
    try
        addpath([opts.mice_path,'lib']);
        addpath([opts.mice_path,'src/mice']);
    catch
        message = 'Missing mice directory';
        logging.error('gen_data', message);
        error(message)
    end
end

