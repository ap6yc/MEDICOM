function ET0 = tle2et(TLE, c)
%TLE2ET - Convert a TLE set to ephemeris
    TLEdatestr = TLE(1,19:32);
    TLEyr = strcat('20',TLEdatestr(1:2));
    TLEdy = TLEdatestr(3:5);
    TLEnm = str2double(TLEdatestr(6:end));
    TLEnm = TLEnm*c.dy2hr;
    TLEhr = floor(TLEnm);
    TLEnm = TLEnm - TLEhr;
    TLEnm = TLEnm*c.hr2mn;
    TLEmn = floor(TLEnm);
    TLEnm = TLEnm - TLEmn;
    TLEsc = TLEnm*c.mn2sc;
    TLEhr = int2str(TLEhr);
    if(length(TLEhr)==1), TLEhr = ['0',TLEhr]; end
    TLEmn = int2str(TLEmn);
    if(length(TLEmn)==1), TLEmn = ['0',TLEmn]; end
    TLEdoystr = [TLEyr,'-',TLEdy,'::',TLEhr,':',TLEmn,':',num2str(TLEsc)];
    ET0 = cspice_str2et(TLEdoystr);
end