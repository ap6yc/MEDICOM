function [dzdt] = eom_tbp_ekf(~,z,GM,M,Q)

r  = z(1:3);
v  = z(4:6);
P  = reshape(z(7:42),6,6);

rdot = v;
vdot = -(GM/norm(r)^3)*r;

G = (GM/norm(r)^5)*(3.0*(r*r') - norm(r)^2*eye(3));
F = [zeros(3,3),eye(3);G,zeros(3,3)];

Pdot = F*P + P*F' + M*Q*M';

dzdt = [rdot;vdot;Pdot(:)];

end