function data = f_gen_data(opts, logger)
%F_GEN_DATA Generates the MEDICOM data
%   INPUTS:
%       opts - the options structure for the project
%       logger - a local logger for diagnostics
%   OUTPUTS:
%       data - a concatenated compilation of the data

% -------------------------------------------------------------------------
% File: gen_data.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 9/27/2018
% Description:
% 
%   This script generates the data necessary for orbit determination and
%   maneuver classification
% 
% -------------------------------------------------------------------------

%% SETUP ENVIRONMENT

logger.info(mfilename, 'Setting up problem');

% Constants
% c.GM   = 3.986004415e5;      % [km^3/s^2]
% c.req  = 6378.137;           % [km] WGS84 value
% c.omg  = 7.2921151467064e-5; % [rad/s] IERS value
% 
% % Conversion constants
% c.asc2deg = 1.0/3600.0;
% c.deg2rad = pi/180.0;
% c.asc2rad = c.asc2deg*c.deg2rad;
% c.rad2asc = 3600.0*180.0/pi;
% c.rad2deg = 180.0/pi;
% c.dy2hr   = 24.0;
% c.hr2mn   = 60.0;
% c.mn2sc   = 60.0;
% c.dy2sc   = 86400.0;
% c.hr2sc   = 3600.0;
c = get_constants();

% Convert time of the TLE to ephemeris time
ET0 = tle2et(opts.TLE, c);
% Specify observer lat/lon (assumed to be on sph surf) -- MAUI GEODSS
phi  =   20.7088*c.deg2rad; % [deg] -> [rad]
lam  = -156.2578*c.deg2rad; % [deg] -> [rad]
slam = sin(lam);
clam = cos(lam);
sphi = sin(phi);
cphi = cos(phi);
% Compute observer position in the fixed frame
rsf  = c.req*[cphi*clam;cphi*slam;sphi];
% Compute the rotation matrix from the fixed frame to the surface frame
Tfs  = [-slam,clam,0.0;-sphi*clam,-sphi*slam,cphi;cphi*clam,cphi*slam,sphi];
% Specify elevation mask
mask = 20.0; % [deg]
% Measurement noise properties
Lk   = diag([3.0;3.0]); % [arcsec]
% Probabilities of failure
pfe  = 0.05; % extreme failure
pfm  = 0.15; % moderate failure

% check the time at the station
[~,~,~,time,~] = cspice_et2lst(ET0, 399, lam, 'PLANETOCENTRIC');

% Convert object from TLE -> KEP -> CAR
KEP = tle2kep(opts.TLE,c.GM);
CAR = kep2car(KEP,c.GM,'rad');

% Perturbation on/off require fundamentally different logic
if opts.PERTURBED
    % Run the "data synthesis loop" (didn't have a better name at the time)
    logger.info(mfilename, 'Entering perturbed synth_loop');
    run('synth_loop.m');
    logger.info(mfilename, 'Exiting perturbed synth_loop');
else
    logger.info(mfilename, 'Beginning unperturbed data gen');
    run('synth_loop_unperturbed.m')
    logger.info(mfilename, 'Exiting unperturbed data gen');
end
    
if opts.PLOT
    figure(3)
    [ax1] = subplot(3,1,1);
    plot(Tm./c.hr2sc,Zm(1,:)*c.asc2deg,'.')
    hold on
    ylabel('RA [deg]')
    [ax2] = subplot(3,1,2);
    plot(Tm./c.hr2sc,Zm(2,:)*c.asc2deg,'.')
    hold on
    ylabel('DEC [deg]')
    [ax3] = subplot(3,1,3);
    plot(Tm./c.hr2sc,EL*c.rad2deg,'.')
    hold on
    ylabel('EL [deg]')
    xlabel('Time [hr]')
    linkaxes([ax1,ax2,ax3],'x')
end

% matlab2tikz('measurements.tex','height','\figureheight','width','\figurewidth','parseStrings',false);

% DATA PROVIDED ARE:
%   Tm = (1 x n) array of observation times [sec]
%   Zm = (2 x n) array of right-ascension and declination observations [arcsec]
%   Rm = (2 x 2 x n) array of measurement noise covariances [arcsec^2]
%   Xt = (6 x n) array of true object position and velocity [km] and [km/s]
%   Rt = (3 x n) array of observer position in inertial frame [km]

% Carve out only the data necessary
for i = 1:length(opts.to_save)
    data.(opts.to_save{i}) = data_all.(opts.to_save{i});
end

end