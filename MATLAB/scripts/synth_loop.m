% -------------------------------------------------------------------------
% File: synth_loop.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 9/27/2018
% Description:
% 
%   This script generates data with maneuvers as perturbations,
%   requiring a rewrite of the original sequential procedure
% 
% -------------------------------------------------------------------------

% try
%     cspice_furnsh([opts.kernels_path,'naif0012.tls.pc'])
%     cspice_furnsh([opts.kernels_path,'pck00010.tpc'])
%     cspice_furnsh([opts.kernels_path,'de430.bsp'])
% catch
%     message = 'Missing kernels directory';
%     opts.logger.error('gen_data', message);
%     error(message)
% end

% Specify a time interval for generating data
tf = opts.t0 + opts.tf*c.dy2sc;
tv = opts.t0:opts.dt:tf;

% Infer the number of data points to iterate against
n_data = length(tv);

% Compute the lat/lon (for ground track) and elevation (for visibility) of the object
lat = zeros(n_data, 1);
lon = zeros(n_data, 1);
ele = zeros(n_data, 1);
tim = zeros(n_data, 1);

% Initialize integration placeholders
TT = zeros(n_data, 1);
XX = zeros(n_data, 6);

% Integration options
int_opts = odeset('AbsTol',1e-9,'RelTol',1e-9);

% Generate synthetic measurement data
ctr = 0;
% Tm  = [];
% Zt  = [];
% Zm  = [];
% Rm  = [];
% Xt  = [];
% Rt  = [];
% EL  = [];
% Preallocate variables dynamically
%TODO: do this dynamically
for j = 1:length(opts.to_save)
    switch opts.to_save{j}
        case {'Tm', 'Tm_c'}
            Tm  = [];             % measurement time [sec]
        case {'Zt', 'Zt_c', 'train'}
            Zt  = [];       % true measurement [rad]
        case {'Zm', 'Zm_c'}
            Zm  = [];             % measurement [arcsec]
        case {'Rm', 'Rm_c'}
            Rm  = [];         % measurement noise covariance [arcsec^2]
        case {'Xt', 'Xt_c'}
            % Xt  = cat(2,Xt,XX(id(i),1:6)'); % true state (pos/vel) [km and km/s]
            Xt  = []; % true state (pos/vel) [km and km/s]
        case {'Rt', 'Rt_c'}
            Rt  = [];            % true inertial observer position [km]
        case {'EL', 'EL_c'}
            % EL  = cat(1,EL,El);
            EL  = [];
%                 case 'train'
%                     Zt  = cat(2, Zt_t, [ra; dc]);       % true measurement [rad]
    end
end


% This simply initializes the storage variable names
for i = 1:length(opts.to_save)
    data_all.(opts.to_save{i}) = [];
end

% Flags
% TODO: implement clouds
TO_PERTURB = false;
DID_PERTURB = false;
IS_VISIBLE = false;

% Track the indices of the measurements against truth
id = [];
junction = [];
i_perturb = [];
data_all.class = [];
cnt = 0;

logger.debug(mfilename, ['Entering loop for ', num2str(n_data), ' iterations']);

% LOOP THROUGH ALL TIME
for i = 1:n_data
    
    logger.trace(mfilename, ['Beginning loop index ', num2str(i)]);
    
    % Propagate object using two-body equations of motion
    if i == 1
        TT(i)          = tv(i);
        XX(i, :)       = CAR';
    else
        tspan      = [tv(i-1), tv(i)];
        [~, x_int] = ode45(@eom_car, tspan, XX(i-1,:), int_opts, c.GM);
        TT(i)      = tv(i);
        XX(i, :)   = x_int(end, :);

        % If the perturbation flag is low and it is the index is correct
        if TO_PERTURB && i == i_perturb_local

            % Perform an instantaneous dV, determined by opts
            XX(i, :) = opts.dv(XX(i,:));

            % Set TO_PERTURB low and indicate perturbation for 'class'
            TO_PERTURB = false;
            DID_PERTURB = true;
        end
    end
    
    % time
%     ti     = TT(i);
    ti = tv(i);
    
    % rotation matrix from inertial frame to fixed frame
    Tif    = cspice_pxform('J2000','IAU_EARTH',ET0+ti);
    % position of object in inertial frame
    roi    = XX(i,1:3)';
    % position of object in fixed frame
    rof    = Tif*roi;
    % lat/lon caclulations
    lat(i) = atan2(rof(3),sqrt(rof(1)*rof(1)+rof(2)*rof(2)))*c.rad2deg;
    lon(i) = atan2(rof(2),rof(1))*c.rad2deg;
    % position of object wrt observer in surface frame
    ross   = Tfs*(Tif*roi - rsf);
    % elevation of object wrt the observer
    ele(i) = asin(ross(3)/norm(ross));
    % determine the time at the station
    [~,~,~,time,~] = cspice_et2lst(ET0+ti,399,lam,'PLANETOCENTRIC');
    % determine if it is night (past 10pm or before 4am)
    if(str2double(time(1:2)) >= 22.0 || str2double(time(1:2)) < 4.0)
        tim(i) = 1.0;
    end

    % if the object is above the elevation mask, continue
    if(ele(i)*c.rad2deg > mask)
        
        % Increment the data-gathering counter
        cnt = cnt + 1;
        
%         dur = zeros(size(nigt));
    % TODO: clouds
%     if opts.CLOUDS
%         if ~FLAG_NIGHT_PICK
%             lav = 120;
%             ldl = 20;
%             len = randi([lav-ldl,lav+ldl]);
%             dur(1) = randi([nigt(1,i) nigt(2,i)-len],1,1);
%             dur(2) = dur(1,i) + len;
%             FLAG_NIGHT_PICK = true;
%         end
%     end
        
        % Append the index of the measurement
        id = [id; i];
        
        % Check for entry
        if ~IS_VISIBLE
            if i > 1
                % Add the index as a junction of measurements
                junction = [junction, cnt];
                
                % Check if perturbation happened
                if DID_PERTURB
                    % Add perturbation to the class, reset the flag
                    data_all.class = [data_all.class; 1];
                    DID_PERTURB = false;
                else
                    % Otherwise, set perturbation low
                    data_all.class = [data_all.class; 0];
                end
            end
            % Reset the flag for entry
            IS_VISIBLE = true;
        end
        
        % position of the observer in the inertial frame
        rsi  = (Tif')*rsf;
        % position of the object wrt the observer in the inertial frame
        rosi = roi - rsi;
        % right-ascension (true)
        ra   = atan2(rosi(2),rosi(1));
        % declination (true)
        dc   = atan2(rosi(3),sqrt(rosi(1)*rosi(1)+rosi(2)*rosi(2)));
        % total measurement
        zi = [ra;dc]*c.rad2asc + Lk*randn(2,1);
        
        % store the data
        ctr = ctr + 1;
        % Store variables dynamically
        for j = 1:length(opts.to_save)
            switch opts.to_save{j}
                case {'Tm', 'Tm_c'}
                    Tm  = cat(1, Tm, ti);             % measurement time [sec]
                case {'Zt', 'Zt_c', 'train'}
                    Zt  = cat(2, Zt, [ra; dc]);       % true measurement [rad]
                case {'Zm', 'Zm_c'}
                    Zm  = cat(2, Zm, zi);             % measurement [arcsec]
                case {'Rm', 'Rm_c'}
                    Rm  = cat(3, Rm, Lk*Lk');         % measurement noise covariance [arcsec^2]
                case {'Xt', 'Xt_c'}
                    % Xt  = cat(2,Xt,XX(id(i),1:6)'); % true state (pos/vel) [km and km/s]
                    Xt  = cat(2, Xt, XX(i, 1:6)'); % true state (pos/vel) [km and km/s]
                case {'Rt', 'Rt_c'}
                    Rt  = cat(2, Rt, rsi);            % true inertial observer position [km]
                case {'EL', 'EL_c'}
                    % EL  = cat(1,EL,El);
                    EL  = cat(1, EL, ele(i));
%                 case 'train'
%                     Zt  = cat(2, Zt_t, [ra; dc]);       % true measurement [rad]
            end
        end
        
    else
        % If we are just entering day/no visibility
        if IS_VISIBLE
            
            % Log the exit out of visibility
            IS_VISIBLE = false;

            % Coin flip to perturb for the unobservable period
            if round(rand())
                % Set the "daytime" perturb index once
                TO_PERTURB = true;
                % TODO: change this rule to be random across the day
%                 i_perturb = i + randi([lav-ldl,lav+ldl]);
                i_perturb_local = i + randi(100);
                i_perturb = [i_perturb, i_perturb_local];
            else
                TO_PERTURB = false;
            end
        end
    end
end % for i = 1:n_data


% Added measurement noise model, FAIL or NOMINAL
if strcmp(opts.noise_mode, 'FAIL')
    c1 = 0;
    c2 = 0;
    c3 = 0;
    for i = 1:length(id)
        % true measurement
        zi = Zt(:,i);
        % probability of failure

        fail = rand;
        % total measurement
        if(fail < pfe && i > 241)
            % extreme measurement noise
            zi = zi*c.rad2asc + 100.0*Lk*randn(2,1);
            c1 = c1 + 1;
        elseif(fail < (pfe + pfm) && i > 241)
            % moderate measurement noise
            zi = zi*c.rad2asc + 10.0*Lk*randn(2,1);
            c2 = c2 + 1;
        else
            % nominal measurement noise
            zi = Zm(:,i);
            c3 = c3 + 1;
        end

        Zm(:,i) = zi;
        
    end

elseif strcmp(opts.noise_mode, 'NOMINAL')
    % nominal measurement noise
    % do nothing (Zm(:,i) = Zm(:,i))
end

% Plot the groundtrack
if opts.PLOT
    figure(2)
    C = get(gca,'ColorOrder');
    plot_groundtrack(lat,lon,'surfmaps/earth4.jpg');
    hold on
    plot(lam*c.rad2deg,phi*c.rad2deg,'p','MarkerSize',10,'MarkerEdgeColor',C(3,:),'MarkerFaceColor',C(3,:))
    % print('GPSgroundtrack','-dpng')
    % matlab2tikz('groundtrack.tex','height','\figureheight','width','\figurewidth','parseStrings',false);
end


% Plot the elevation
if opts.PLOT
    figure(3)
    C = get(gca,'ColorOrder');
    XL = [0,10];
    YL = [-75,75];
    idxs = find(tim == 0.0);
    didx = find(diff(idxs) > 1);
    ibeg = idxs(1);
    for i = 1:length(didx)
        iend = idxs(didx(i));
        rectangle('Position',[TT(ibeg)./c.dy2sc,YL(1),(TT(iend)-TT(ibeg))./c.dy2sc,YL(2)-YL(1)],'FaceColor',[250 250 210]./255,'EdgeColor','none')
        ibeg = idxs(didx(i)+1);
    end
    iend = idxs(end);
    rectangle('Position',[TT(ibeg)./c.dy2sc,YL(1),(TT(iend)-TT(ibeg))./c.dy2sc,YL(2)-YL(1)],'FaceColor',[250 250 210]./255,'EdgeColor','none')
    rectangle('Position',[XL(1),YL(1),XL(2)-XL(1),mask-YL(1)],'FaceColor',[0.9 0.9 0.9],'EdgeColor','none')
    hold on
    plot(TT./c.dy2sc,ele*c.rad2deg,'Color',C(1,:),'LineWidth',1.2);
    % plot(TT./c.dy2sc,45*tim,'Color',[0,0,0],'LineWidth',1.2);
    box on
    xlim([0,10])
    ylim([-75,75])
    % matlab2tikz('elevation.tex','height','\figureheight','width','\figurewidth','parseStrings',false);
end

% Plot the orbit in the inertial frame
if opts.PLOT
    figure(1)
    plot_3Dorbit(XX(:,1),XX(:,2),XX(:,3),c.req,'surfmaps/earth4.jpg');
    view(177,34)
    axis off
    % print('GPSorbit','-dpng')
end

%%

% Initialize variables dynamically
% TODO: programmatically preallocate here, mega time savings
% for i = 1:length(opts.to_save)
%     switch opts.to_save{i}
%         case 'Tm_c'
%             data_all.Tm_c = [];
%         case 'Zt_c'
%             data_all.Zt_c = [];
%         case 'Zm_c'
%             data_all.Zm_c = [];
%         case 'Rm_c'
%             data_all.Rm_c = [];
%         case 'Xt_c'
%             data_all.Xt_c = [];
%         case 'Rt_c'
%             data_all.Rt_c = [];
%         case 'EL_c'
%             data_all.EL_c = [];
%         case 'train'
%             data_all.train = [];
%     end
% end

% disp(id)

% disp(junction)
% disp(cnt)
% disp(size(Tm))

% Organize for IOD
pre_cnt = 1;
if opts.IOD
    % Iterate against each 'junction' between measurement arcs
    for k = 1:length(junction)
        i = junction(k);
        % Skip the first entry (or any with not enough data)
        if i > opts.n_cut && i < cnt - opts.n_cut
%             store_cnt  = store_cnt  + 1;
%             cut_range = i - opts.n_cut:i + opts.n_cut - 1;
%             disp('pre_cnt:')
%             disp(pre_cnt)
%             disp('Junction i - 1:')
%             disp(i-1)
            % Cut variables dynamically
%             for j = 1:length(opts.to_save)
%                 switch opts.to_save{j}
%                     case {'Tm'}
%                         data_all.Tm  = cat(1, data_all.Tm, Tm(pre_cnt:i-1));  % measurement time [sec]
%                     case {'Zt'}
%                         data_all.Zt  = cat(2, data_all.Zt, Zt(:,pre_cnt:i-1));  % true measurement [rad]
% %                         Zt  = cat(2, Zt, [ra; dc]);
%                     case {'Zm'}
%                         data_all.Zm  = cat(2, data_all.Zm, Zm(:,pre_cnt:i-1));  % measurement [arcsec]
%                     case {'Rm'}
%                         data_all.Rm  = cat(3, data_all.Rm, Rm(:, :, pre_cnt:i-1));  % measurement noise covariance [arcsec^2]
%                     case {'Xt'}
%                         data_all.Xt  = cat(2, data_all.Xt, Xt(:,pre_cnt:i-1));  % true state (pos/vel) [km and km/s]
%                     case {'Rt'}
%                         data_all.Rt  = cat(2, data_all.Rt, Rt(:,pre_cnt:i-1));  % true inertial observer position [km]
%                     case {'EL'}
%                         data_all.EL  = cat(1, data_all.Zm, Zm(pre_cnt:i-1)); % elevation
%                 end
            for j = 1:length(opts.to_save)
                switch opts.to_save{j}
                    case {'Tm'}
                        data_all.Tm{k} = Tm(pre_cnt:i-1);  % measurement time [sec]
                    case {'Zt'}
                        data_all.Zt{k} = Zt(:,pre_cnt:i-1);  % true measurement [rad]
                    case {'Zm'}
                        data_all.Zm{k} = Zm(:,pre_cnt:i-1);  % measurement [arcsec]
                    case {'Rm'}
                        data_all.Rm{k} = Rm(:, :, pre_cnt:i-1);  % measurement noise covariance [arcsec^2]
                    case {'Xt'}
                        data_all.Xt{k} = Xt(:,pre_cnt:i-1);  % true state (pos/vel) [km and km/s]
                    case {'Rt'}
                        data_all.Rt{k} = Rt(:,pre_cnt:i-1);  % true inertial observer position [km]
                    case {'EL'}
                        data_all.EL{k} = Zm(pre_cnt:i-1); % elevation
                    case{'Zmfp1'}
                        data_all.Zmfp1{k} = Zm(:, i);
                    case{'Tmfp1'}
                        data_all.Tmfp1{k} = Tm(i);
                    case{'Rtfp1'}
                        data_all.Rtfp1{k} = Rt(:, i);
                    case{'class_IOD'}
                        data_all.class_IOD{k} = data_all.class(k);
                end

%                 case {'Tm', 'Tm_c'}
%                     Tm  = cat(1, Tm, ti);             % measurement time [sec]
%                 case {'Zt', 'Zt_c', 'train'}
%                     Zt  = cat(2, Zt, [ra; dc]);       % true measurement [rad]
%                 case {'Zm', 'Zm_c'}
%                     Zm  = cat(2, Zm, zi);             % measurement [arcsec]
%                 case {'Rm', 'Rm_c'}
%                     Rm  = cat(3, Rm, Lk*Lk');         % measurement noise covariance [arcsec^2]
%                 case {'Xt', 'Xt_c'}
%                     % Xt  = cat(2,Xt,XX(id(i),1:6)'); % true state (pos/vel) [km and km/s]
%                     Xt  = cat(2, Xt, XX(i, 1:6)'); % true state (pos/vel) [km and km/s]
%                 case {'Rt', 'Rt_c'}
%                     Rt  = cat(2, Rt, rsi);            % true inertial observer position [km]
%                 case {'EL', 'EL_c'}
%                     % EL  = cat(1,EL,El);
%                     EL  = cat(1, EL, ele(i));

            end
        end
        pre_cnt = i;
    end
else
    % Store variables dynamically
    for j = 1:length(opts.to_save)
        switch opts.to_save{j}
            case {'Tm'}
                data_all.Tm  = Tm;  % measurement time [sec]
            case {'Zt'}
                data_all.Zt  = Zt;  % true measurement [rad]
            case {'Zm'}
                data_all.Zm  = Zm;  % measurement [arcsec]
            case {'Rm'}
                data_all.Rm  = Rm;  % measurement noise covariance [arcsec^2]
            case {'Xt'}
                data_all.Xt  = Xt;  % true state (pos/vel) [km and km/s]
            case {'Rt'}
                data_all.Rt  = Rt;  % true inertial observer position [km]
            case {'EL'}
                data_all.EL  = EL; % elevation
        end
    end
end

%% Cut and store classification data
% NOTE: the variable 'class' is already recorded in the n_data loop

% Store count iterator
store_cnt = 0;

% Iterate against each 'junction' between measurement arcs
for i = junction
    
    % Skip the first entry (or any with not enough data)
    if i > opts.n_cut && i < cnt - opts.n_cut
        store_cnt  = store_cnt  + 1;
        cut_range = i - opts.n_cut:i + opts.n_cut - 1;
        
        % Cut variables dynamically
        for j = 1:length(opts.to_save)
            switch opts.to_save{j}
                case 'Tm_c'
                    % measurement time [sec]
                    % Tm  = cat(1,Tm,ti);             
                    data_all.Tm_c = cat(1, data_all.Tm_c, Tm(cut_range));
                case 'Zt_c'
                    % true measurement [rad]
                    % Zt  = cat(2,Zt,[ra;dc]);
                    data_all.Zt_c  = cat(2, data_all.Zt_c, Zt(:, cut_range));
                case 'Zm_c'
                    % measurement [arcsec]
                    % Zm  = cat(2,Zm,zi);
                    data_all.Zm_c  = cat(2, data_all.Zm_c, Zm(:, cut_range));
                case 'Rm_c'
                    % Rm  = cat(3,Rm,Lk*Lk');         % measurement noise covariance [arcsec^2]
                    data_all.Rm_c = [];
                case 'Xt_c'
                    data_all.Xt_c = [];
                    % Xt  = cat(2,Xt,XX(i,1:6)'); % true state (pos/vel) [km and km/s]
                case 'Rt_c'
                    % Rt  = cat(2,Rt,rsi);            % true inertial observer position [km]
                    data_all.Rt_c = [];
                case 'EL_c'
                    % EL  = cat(1,EL,ele(i));
                    data_all.EL_c = [];
                case 'train'
                    % train(store_cnt ,:, :) = Zt(:, cut_range);
                    data_all.train(store_cnt, :) = [Zt(1, cut_range), Zt(2, cut_range)];
            end
        end
    end
end