% Specify a time interval for generating data
t0 =  0.0;
dt = 10.0;
tf = t0 + 10.0*c.dy2sc;
tv = t0:dt:tf;
% Propagate object using two-body equations of motion
opt     = odeset('AbsTol',1e-9,'RelTol',1e-9);
[TT,XX] = ode45(@eom_car,tv,CAR,opt,c.GM);

% Plot the orbit in the inertial frame
if opts.PLOT
    figure(1)
    plot_3Dorbit(XX(:,1),XX(:,2),XX(:,3),c.req,'surfmaps/earth4.jpg');
    view(177,34)
    axis off
    % print('GPSorbit','-dpng')
end

% Compute the lat/lon (for ground track) and elevation (for visibility) of the object
lat = zeros(length(TT),1);
lon = zeros(length(TT),1);
ele = zeros(length(TT),1);
tim = zeros(length(TT),1);
for i = 1:length(TT)
    % time
    ti     = TT(i);
    % rotation matrix from inertial frame to fixed frame
    Tif    = cspice_pxform('J2000','IAU_EARTH',ET0+ti);
    % position of object in inertial frame
    roi    = XX(i,1:3)';
    % position of object in fixed frame
    rof    = Tif*roi;
    % lat/lon caclulations
    lat(i) = atan2(rof(3),sqrt(rof(1)*rof(1)+rof(2)*rof(2)))*c.rad2deg;
    lon(i) = atan2(rof(2),rof(1))*c.rad2deg;
    % position of object wrt observer in surface frame
    ross   = Tfs*(Tif*roi - rsf);
    % elevation of object wrt the observer
    ele(i) = asin(ross(3)/norm(ross));
    % determine the time at the station
    [~,~,~,time,~] = cspice_et2lst(ET0+ti,399,lam,'PLANETOCENTRIC');
    % determine if it is night (past 10pm or before 4am)
    if(str2double(time(1:2)) >= 22.0 || str2double(time(1:2)) < 4.0)
        tim(i) = 1.0;
    end
end

% Plot the groundtrack
if opts.PLOT
    figure(2)
    C = get(gca,'ColorOrder');
    plot_groundtrack(lat,lon,'surfmaps/earth4.jpg');
    hold on
    plot(lam*c.rad2deg,phi*c.rad2deg,'p','MarkerSize',10,'MarkerEdgeColor',C(3,:),'MarkerFaceColor',C(3,:))
    % print('GPSgroundtrack','-dpng')
    % matlab2tikz('groundtrack.tex','height','\figureheight','width','\figurewidth','parseStrings',false);
end


% Plot the elevation
if opts.PLOT
    figure(3)
    C = get(gca,'ColorOrder');
    XL = [0,10];
    YL = [-75,75];
    idxs = find(tim == 0.0);
    didx = find(diff(idxs) > 1);
    ibeg = idxs(1);
    for i = 1:length(didx)
        iend = idxs(didx(i));
        rectangle('Position',[TT(ibeg)./c.dy2sc,YL(1),(TT(iend)-TT(ibeg))./c.dy2sc,YL(2)-YL(1)],'FaceColor',[250 250 210]./255,'EdgeColor','none')
        ibeg = idxs(didx(i)+1);
    end
    iend = idxs(end);
    rectangle('Position',[TT(ibeg)./c.dy2sc,YL(1),(TT(iend)-TT(ibeg))./c.dy2sc,YL(2)-YL(1)],'FaceColor',[250 250 210]./255,'EdgeColor','none')
    rectangle('Position',[XL(1),YL(1),XL(2)-XL(1),mask-YL(1)],'FaceColor',[0.9 0.9 0.9],'EdgeColor','none')
    hold on
    plot(TT./c.dy2sc,ele*c.rad2deg,'Color',C(1,:),'LineWidth',1.2);
    % plot(TT./c.dy2sc,45*tim,'Color',[0,0,0],'LineWidth',1.2);
    box on
    xlim([0,10])
    ylim([-75,75])
    % matlab2tikz('elevation.tex','height','\figureheight','width','\figurewidth','parseStrings',false);
end

% find the time indices where it is night at the station
idxs = find(tim > 0.0 & ele*c.rad2deg > mask);
didx = find(diff(idxs) > 1);
nigt = zeros(2,length(didx)+1);
nigt(1,1) = idxs(1);
nigt(2,1) = idxs(didx(1));
for i = 2:length(didx)
    nigt(1,i) = idxs(didx(i-1)+1);
    nigt(2,i) = idxs(didx(i));
end
nigt(1,i+1) = idxs(didx(i)+1);
nigt(2,i+1) = idxs(end);

% for each night, pick a random starting index
dur = zeros(size(nigt));
lav = 120;
ldl = 20;
for i = 1:size(nigt,2)
    len = randi([lav-ldl,lav+ldl]);
    dur(1,i) = randi([nigt(1,i) nigt(2,i)-len],1,1);
    dur(2,i) = dur(1,i) + len;
end

% determine the indices of the measurements
id = [];
for i = 1:size(dur,2)
    id = cat(1,id,(dur(1,i):dur(2,i))');
end

% Generate synthetic measurement data
ctr = 0;
Tm  = [];
Zt  = [];
Zm  = [];
Rm  = [];
Xt  = [];
Rt  = [];
EL  = [];
for i = 1:length(id)
    % time
    ti     = TT(id(i));
    % rotation matrix from inertial frame to fixed frame
    Tif    = cspice_pxform('J2000','IAU_EARTH',ET0+ti);
    % position of object in inertial frame
    roi    = XX(id(i),1:3)';
    % position of object wrt observer in surface frame
    ross   = Tfs*(Tif*roi - rsf);
    % elevation of object wrt the observer
    El     = asin(ross(3)/norm(ross));
    % if the object is above the elevation mask, continue
    if(El*c.rad2deg > mask)
        % position of the observer in the inertial frame
        rsi  = (Tif')*rsf;
        % position of the object wrt the observer in the inertial frame
        rosi = roi - rsi;
        % right-ascension (true)
        ra   = atan2(rosi(2),rosi(1));
        % declination (true)
        dc   = atan2(rosi(3),sqrt(rosi(1)*rosi(1)+rosi(2)*rosi(2)));
        % total measurement
        zi = [ra;dc]*c.rad2asc + Lk*randn(2,1);
        % store the data
        ctr = ctr + 1;
        Tm  = cat(1,Tm,ti);             % measurement time [sec]
        Zt  = cat(2,Zt,[ra;dc]);        % true measurement [rad]
        Zm  = cat(2,Zm,zi);             % measurement [arcsec]
        Rm  = cat(3,Rm,Lk*Lk');         % measurement noise covariance [arcsec^2]
        Xt  = cat(2,Xt,XX(id(i),1:6)'); % true state (pos/vel) [km and km/s]
        Rt  = cat(2,Rt,rsi);            % true inertial observer position [km]
        EL  = cat(1,EL,El);
    else
        disp(El*c.rad2deg)
    end
end

if strcmp(opts.noise_mode, 'FAIL')
    c1 = 0;
    c2 = 0;
    c3 = 0;
    for i = 1:length(id)
        % true measurement
        zi = Zt(:,i);
        % probability of failure

        fail = rand;
        % total measurement
        if(fail < pfe && i > 241)
            % extreme measurement noise
            zi = zi*c.rad2asc + 100.0*Lk*randn(2,1);
            c1 = c1 + 1;
        elseif(fail < (pfe + pfm) && i > 241)
            % moderate measurement noise
            zi = zi*c.rad2asc + 10.0*Lk*randn(2,1);
            c2 = c2 + 1;
        else
            % nominal measurement noise
            zi = Zm(:,i);
            c3 = c3 + 1;
        end

        Zm(:,i) = zi;

    end

elseif strcmp(opts.noise_mode, 'NOMINAL')
    % nominal measurement noise
    % do nothing (Zm(:,i) = Zm(:,i))
end