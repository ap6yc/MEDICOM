% -------------------------------------------------------------------------
% File: test_iteration.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 11/28/2018
% Description:
% 
%   This script is the main implementation for data generation for maneuver
%   classification
% 
% -------------------------------------------------------------------------


%% SHALLOW SETUP
%   This section sets up the initial path, gets this filename, etc.

clear all;
close all;
clc;

cd(fileparts(which(mfilename)))

% Point to the options file
opts_path = '../../data/opts.yml';

% Get the context manager
CM = ContextManager(opts_path);

% Get the options from the context manager
opts = CM.get_opts();


%% DEEP SETUP
%   This section generates the options, initializes the debugger, and loads
%   the SPICE kernels

% Generate the options for the program
% opts = genOpts();

opts.logger.info(mfilename, 'Generating options');

% CSPICE setup
micePath(opts, opts.logger);

% Serial setup
if ~opts.RUN_PARALLEL
    furnish(opts, opts.logger);
end

% Set a random number generator seed
rng(opts.rand_seed);

% Change plotting options to latex interpreter
% set(groot, 'defaultAxesTickLabelInterpreter','latex');
% set(groot, 'defaultLegendInterpreter','latex');
% set(groot, 'defaultTextInterpreter','latex');


%% DO THE EVERYTHING
%   This section does the everything

t_main = tic;

% Create a separate logger for the parallel instance
logger_local = opts.get_logger(['P', num2str(i)])

% Furnish for this job
furnish(opts, logger_local);

% Gen that data
% logger_local.info(mfilename, ['Entering data generation for parallel iteration ', num2str(i)]);
data = f_gen_data(opts, logger_local);
% logger_local.info(mfilename, ['Ending data generation for parallel iteration ', num2str(i)]);

% % Reshuffle the data, TODO: fix this hack
% data_cat.train = reshuffle_detection(data_cat.train);
% 
% % Save the data as a struct
% save(opts.save_full, '-struct','data_cat');
% 
% % Save the copy
% save(opts.save_copy, '-struct','data_cat');
% 
% % Compute those covariances
% % opts.logger.info(mfilename, 'Entering EKF');
% % run('EKF.m')
% 
% %% CLOSE AND CLEAN UP

% Diagnostic
disp('Successful data generation')
opts.logger.info(mfilename, 'Ending data generation');

t_main_end = toc(t_main)