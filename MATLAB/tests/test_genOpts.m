% -------------------------------------------------------------------------
% File: test_genOpts.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 10/25/2018
% Description:
% 
%   This script is the main implementation for data generation for maneuver
%   classification
% 
% -------------------------------------------------------------------------


%% SHALLOW SETUP
%   This section sets up the initial path, gets this filename, etc.

% Clean up environment
clc
clear all
close all

% Get to top-path
cd(fileparts(which(mfilename)))
cd('..')

%% TEST

% Point to the options file
opts_path = '../data/opts.yml';

% Get the context manager
CM = ContextManager(opts_path);

% Get the options out
opts = CM.get_opts();

% Initialize the parallel pool, if applicable
% opts.logger.info(mfilename, 'Initializing pool');
% [poolobj, poolsize] = initPool(opts.RUN_PARALLEL);

% Diagnostics
disp([mfilename, ' success'])
