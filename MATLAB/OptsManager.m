classdef OptsManager < handle
    %OPTSMANAGER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        opts
        to_remove
    end
    
    methods
        function obj = OptsManager(opts_path)
            %OPTSMANAGER Construct an instance of OptsManager
            %   INPUTS:
            %       opts_path - full path to options YAML file
            %   OUTPUTS:
            %       obj - instance of OptsManager

            % Manually add YAMLMatlab
            ContextManager.gen_paths({'YAMLMatlab'})
            
%             obj.path = OptsManager.gen_opts_path();
%             obj.opts = get_opts('new');
            obj.opts = obj.load_opts(opts_path);
            
            % Deconstruct the options structs
            obj.deconstruct_MATLAB_opts();
            
            % Parse the options
%             obj.parse_opts();
            
            obj.to_remove = {'get_logger', 'dv','save_file','save_full',...
                    'inst_opts_full','opts_full','logger','TLE'};
        end
        
        function opts = get_opts(obj)
            %GET_OPTS Getter for options
            %   INPUTS:
            %       N/A
            %   OUTPUTS:
            %       opts - options struct
            
            opts = obj.opts;
            
            % TODO: implement dynamic loading here
            
%             switch old_new
%                 case 'old'
%                     opts = ReadYaml(opts_full);
%                 case 'new'
%                     opts = ReadYaml(opts_full);
%             end
        end
        
        function parse_opts(obj)
            %PARSE_OPTS Parses the options by reformatting and appending
            %relevant MATLAB inline functions, etc.
            %   INPUTS:
            %       N/A
            %   OUTPUTS:
            %       N/A

            % Delta-V model
            obj.opts.dv = @(x) dV(x, obj.opts);
            
            % Logger generator function
            obj.opts.get_logger = @(pre) get_logger(pre, obj.opts);
            
            % TLE pre-parsing
            obj.opts.TLE(1, :) = obj.opts.TLE_1;
            obj.opts.TLE(2, :) = obj.opts.TLE_2;
            
            % Full naming options
            obj.opts.save_file  = [obj.opts.save_template, ...
                datestr(now, obj.opts.date_format)]; % Name of final data file
            obj.opts.save_full = [obj.opts.data_path, '/', ...
                obj.opts.detection_dir, '/', obj.opts.save_file];
            obj.opts.save_copy = [obj.opts.copy_path, '/', ...
                obj.opts.save_file];
            
%             opts.inst_opts_full = [opts.data_path, '/', opts.inst_opts_path, '/', ...
%                 obj.opts.inst_opts_name, datestr(now, opts.date_format), '.yml'];
%             obj.opts.opts_full = [opts.data_path, '/', opts.opts_name];
            
            % Logger location
            obj.opts.logger = obj.opts.get_logger('main');
            
        end % parse_opts
        
        function save_opts(obj)
            %SAVE_OPTS Save the current options configuration
            %   INPUTS:
            %       N/A
            %   OUTPUTS:
            %       N/A
            
            % Carve out function handles and save the options configuration
            opts_to_save = obj.opts;
            opts_to_save = rmfield(opts_to_save, obj.to_remove);
            
            % Generate a pathname and save
            path = obj.gen_opts_path();
            obj.write_opts(opts_to_save, path);
            
        end
        
        function deconstruct_MATLAB_opts(obj)
            %DECONSTRUCT_MATLAB_OPTS Parses options sorted under MATLAB in
            %the options struct
            %    This was implemented because refactoring every one of the
            %    existing MATLAB flags, switch cases, etc. would be a
            %    nightmare (and for no good reason to boot). Still, the
            %    options file is messy without this categorization, so this
            %    is the solution. Please forgive me.
            %
            %    INPUTS:
            %        N/A
            %    OUTPUTS:
            %        N/A

            obj.opts = OptsManager.destructify(obj.opts.MATLAB, obj.opts);
            
        end % deconstruct_MATLAB_opts
        
    end % methods
    
    methods (Static)
        
        function target = destructify(source, target)
            %DESTRUCTIFY Breaks down a multi-level struct to a flat struct
            %   NOTE:
            %       This function is not tolerant of multiple levels having
            %       the same name, as handling such behavior is
            %   INPUTS:
            %       source - multi-level struct to grab all fields from
            %       target - struct to assign the fields in
            %   OUTPUTS:
            %       N/A
            
            % Get the field names of the source struct
            fnames = fieldnames(source);
            
            for i = 1:length(fnames)

                % Take out the i'th field
                local_name = fnames{i};
                local_field = source.(local_name);

                % Check if the i'th field of S is a struct
                if isstruct(local_field)
                    % If local_field is a struct, recursive function call
                    target = OptsManager.destructify(local_field, target); % Call self
                else
                    % Otherwise, assign the field to the target
                    target.(local_name) = local_field;
                end
            end
            
        end % destructify
        
        function path = gen_opts_path()
            %GEN_OPTS_PATH Generate an enumerated path for saving options
            %   INPUTS:
            %       N/A
            %   OUTPUTS:
            %       path - unique/enumerated path for option saving
            
            % TODO change this path
            opts_path = '../data/configs/';
            path = [opts_path, 'opts', datestr(now, opts.date_format), '.yml'];
            
        end % gen_opts_path
        
        function opts = load_opts(path)
            %LOAD_OPTS asdf
            %   INPUTS:
            %       path - string pointing to options full path
            %   OUTPUTS:
            %       opts - full opts structure

            % Load the options, assuming it to be a YAML file
            opts = ReadYaml(path);
            
        end % load_opts
        
        function write_opts(opts, path)
            %WRITE_OPTS Writing operations (YAML) for an options struct
            %   INPUTS:
            %       opts - options struct to be saved,
            %              must contain no function handles,
            %              dynamic types, etc.
            %       path - full path for saving the options,
            %              must be relative to top-level
            
            WriteYaml(path, opts);
            
        end % write_opts
        
    end
end

