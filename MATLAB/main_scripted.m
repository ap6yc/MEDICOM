% -------------------------------------------------------------------------
% File: main_scripted.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 9/27/2018
% Description:
% 
%   This script is the main implementation for data generation for maneuver
%   classification
% 
%   NOTE: this implementation is depricated, and it is maintained for
%   historical and developmental purposes
% 
% -------------------------------------------------------------------------


%% INITIAL SETUP
%   This section sets up the initial path, gets this filename, etc.

% Clean up environment
clear all;
close all;
clc;

% Name of this file
localFilename = 'main.m';

% Build the path
% run('buildPath.m')
os_error_msg = 'Where''d you get this OS, the toilet store?';

% Construct path
if ispc
    cd(fileparts(which(localFilename)));
    top_path = pwd;
elseif isunix
    localFolder = '~/MEDICOM/MATLAB/';
    old_path = cd(localFolder);
    top_path = pwd;
else
    error(os_error_msg)
end

% Add the paths
% addpath(genpath(top_path));
addpath(genpath('utilities'));
addpath(genpath('scripts'));
addpath(genpath('classes'));
addpath(genpath('surfmaps'));

%% DEEP SETUP
%   This section generates the options, initializes the debugger, and loads
%   the SPICE kernels

% Generate the options for the program
opts = genOpts();

% Logger location
logger = log4m.getLogger(opts.log_full);
logger.setCommandWindowLevel(logger.INFO);
logger.setLogLevel(logger.INFO);
% logger.setLogLevel(L.ERROR);
% TRACE, DEBUG, INFO, WARN, ERROR, FATAL

% To log an error event
% logger.error('exampleFunction','An error occurred');

% To log a trace event 
% logger.trace('function','Trace this event');

logger.info('main', 'Generating options');

% Add the mice paths
logger.debug('main', 'Adding mice paths');
try
    addpath([opts.mice_path,'lib']);
    addpath([opts.mice_path,'src/mice']);
catch
    message = 'Missing mice directory';
    logging.error('gen_data', message);
    error(message)
end

% Load the kernels
logger.debug('gen_data', 'Adding kernels paths');
try
    cspice_furnsh([opts.kernels_path,'naif0012.tls.pc'])
    cspice_furnsh([opts.kernels_path,'pck00010.tpc'])
    cspice_furnsh([opts.kernels_path,'de430.bsp'])
catch
    message = 'Missing kernels directory';
    logger.error('gen_data', message);
    error(message)
end


%% START PARALLEL POOL
%   This section initializes the parallel pool if necessary (or applicable)

% Initialize the parallel pool, if applicable
logger.info('main', 'Initializing pool');
[poolobj,poolsize] = initPool(opts.RUN_PARALLEL);

%% DO THE EVERYTHING
%   This section does the everything

% Iterate against each individual
if opts.RUN_PARALLEL
    
    % Add counting and failure flag for the parallel loop
    count = 0;
    flag_failure = 1;
    
    while flag_failure
        
        try
            logger.info('main', 'Beginning parallel loop');
            parfor i = 1:opts.n_runs
                % WORK HERE
%                 % Gen that data
%                 logger.info('main', 'Entering data generation');
%                 run('gen_data.m')
            end
            % Reset the failure flag
            flag_failure = 0;
        catch
            
            % Log and warn
            message = 'Parallel job failed, restarting';
            logger.warning('main', message);
            warning(message)
            % warning('Parallel job %u failed, restarting',index_individual)
            
            count = count + 1;
            if count > opts.max_retry_count
                break
            end
        end
    end
    
    % Report failure
    if flag_failure
        message = 'Complete generation failure';
        logger.warning('main', message)
        warning(message)
    end % if flag_failure
    
% Else, run serially
else
    % Add counting and failure flag for the serial loop
    count = 0;
    flag_failure = 1;
    
    while flag_failure
        
        try
            logger.info('main', 'Beginning serial loop');
            for i = 1:opts.n_runs
                % WORK HERE
            end
            
        catch
            
            % Log and warn
            message = ['Serial job ', index_individual,' failed, restarting'];
            logger.warning('main', message);
            warning(message)
            count = count + 1;
            if count > opts.max_retry_count
                break
            end
            
        end
        
    end % while flag_failure
    
    % Report failure
    if flag_failure
        message = 'Complete generation failure';
        logger.warning('main', message);
        warning(message)
    end
    
end % if opts.RUN_PARALLEL

% Gen that data
logger.info('main', 'Entering data generation');
run('gen_data.m')

% Compute those covariances
% logger.info('main', 'Entering EKF');
% run('EKF.m')

%% CLOSE AND CLEAN UP

% Close the pool if run in parallel
if opts.RUN_PARALLEL && opts.CLOSE_POOL
    logger.info('Closing parallel pool');
    delete(poolobj)
end

% Diagnostic
disp('Successful data generation')
logger.info('main', 'Ending data generation');