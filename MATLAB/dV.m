function x_new = dV(x, opts)
%DV Impulsive delta V model for MEDICOM
% 
%   INPUTS:
%       x       - the state at which to perform the maneuver
%       opts    - the MEDICOM simulation options structure
%   OUTPUTS:
%       x_new   - new state with additive delta_v applied impulsively

    % Determine the unit-vector direction of the dv
    switch opts.MAN_DIRECTION
        case 'PLANAR'
            direction = x(4:6)/norm(x(4:6));
        case 'RANDOM'
            az = 2*(rand()-1)*pi;
            el = 2*(rand()-1)*pi/2;
            direction = sph2cart(az, el, 1);
    end
    
    % Determine scaling of the dv
    switch opts.MAN_MAGNITUDE
        case 'FIXED'
            delta_v = direction * opts.dv_max;
        case 'RANDOM'
            delta_v = direction * (2*rand()-1) * opts.dv_max;
    end
    
    x_new = x + [zeros(1,3), delta_v];
end