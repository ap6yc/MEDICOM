% -------------------------------------------------------------------------
% File: simple_main.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 10/25/2018
% Description:
% 
%   This script is the "simple" main implementation for data generation
%   for maneuver classification
% 
% -------------------------------------------------------------------------


%% SHALLOW SETUP
%   This section sets up the initial path, gets this filename, etc.

clear all;
close all;
clc;

cd(fileparts(which(mfilename)))

% Point to the options file
opts_path = '../data/opts.yml';

% Get the context manager
CM = ContextManager(opts_path);

% Get the options from the context manager
opts = CM.get_opts();


%% DEEP SETUP
%   This section generates the options, initializes the debugger, and loads
%   the SPICE kernels

% Generate the options for the program
% opts = genOpts();

opts.logger.info(mfilename, 'Generating options');

% CSPICE setup
micePath(opts, opts.logger);

% Serial setup
if ~opts.RUN_PARALLEL
    furnish(opts, opts.logger);
end

% Set a random number generator seed
rng(opts.rand_seed);

% Change plotting options to latex interpreter
% set(groot, 'defaultAxesTickLabelInterpreter','latex');
% set(groot, 'defaultLegendInterpreter','latex');
% set(groot, 'defaultTextInterpreter','latex');

%% START PARALLEL POOL
%   This section initializes the parallel pool if necessary (or applicable)

% Initialize the parallel pool, if applicable
opts.logger.info(mfilename, 'Initializing pool');
[poolobj, poolsize] = initPool(opts.RUN_PARALLEL);


t_main = tic;

% Division of labor
[data, stats] = dol(@job_gen_data, opts);

timers = stats.timers;

for i = 1:opts.n_runs
    % Time the local iteration
    local_timer = tic;

    % Create a separate logger for the parallel instance
    logger_local = opts.get_logger(['P', num2str(i)]);

    % Furnish for this job
    furnish(opts, logger_local);
    
    % Gen that data
    logger_local.info(mfilename, ['Entering data generation for iteration ', num2str(i)]);
    data = f_gen_data(opts, logger_local);
    logger_local.info(mfilename, ['Ending data generation for iteration ', num2str(i)]);

    % End the timer, store the time
    timer = toc(local_timer);
end
