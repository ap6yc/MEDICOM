% -------------------------------------------------------------------------
% File: main.m
% 
% Editor: Sasha Petrenko
% Professor: Dr. Kyle DeMars
% Date: 10/25/2018
% Description:
% 
%   This script is the main implementation for data generation for maneuver
%   classification
% 
% -------------------------------------------------------------------------


%% SHALLOW SETUP
%   This section sets up the initial path, gets this filename, etc.

clear all;
close all;
clc;

cd(fileparts(which(mfilename)))

% Point to the options file
opts_path = '../data/opts.yml';

% Get the context manager
CM = ContextManager(opts_path);

% Get the options from the context manager
opts = CM.get_opts();


%% DEEP SETUP
%   This section generates the options, initializes the debugger, and loads
%   the SPICE kernels

% Generate the options for the program
% opts = genOpts();

opts.logger.info(mfilename, 'Generating options');

% CSPICE setup
micePath(opts, opts.logger);

% Serial setup
if ~opts.RUN_PARALLEL
    furnish(opts, opts.logger);
end

% Set a random number generator seed
rng(opts.rand_seed);

% Change plotting options to latex interpreter
% set(groot, 'defaultAxesTickLabelInterpreter','latex');
% set(groot, 'defaultLegendInterpreter','latex');
% set(groot, 'defaultTextInterpreter','latex');

%% START PARALLEL POOL
%   This section initializes the parallel pool if necessary (or applicable)

% Initialize the parallel pool, if applicable
opts.logger.info(mfilename, 'Initializing pool');
[poolobj, poolsize] = initPool(opts.RUN_PARALLEL);

%% DO THE EVERYTHING
%   This section does the everything

t_main = tic;

% Division of labor
[data, stats] = dol(@job_gen_data, opts);

timers = stats.timers;

%%

% Concatenate the data
if opts.IOD
    
    cnt = 1;
    data_cat = cell(1, opts.n_runs * length(data{1}.(opts.to_save{1})));
    for i = 1:opts.n_runs 
        for k = 1:length(data{i}.(opts.to_save{1}))
            for j = 1:length(opts.to_save)
                data_cat{cnt}.(opts.to_save{j}) = data{i}.(opts.to_save{j}){k};
            end
            cnt = cnt + 1;
        end
    end
else
    % Initialize the concatenated structure for saving
    for i = 1:length(opts.to_save)
        data_cat.(opts.to_save{i}) = [];
    end
    
    for i = 1:length(data)
        for j = 1:length(opts.to_save)
            data_cat.(opts.to_save{j}) = [data_cat.(opts.to_save{j}); data{i}(opts.to_save{j})];
        end
    end
    
end

%% IOD
% This section does IOD on each "pre" arc, getting the Mahalanobis distance
% of the next observation at the next arc

% Run scripted IOD
if opts.IOD
%     run('IOD_script.m');
    [data_iod, stats_iod] = dol_IOD(@job_IOD, opts, data_cat);
end
%%
% Concatenate the iod data for ease
data_iod_cat.d = [];
data_iod_cat.class = [];
for i = 1:length(data_iod)
    if ~isempty(data_iod{i})
        data_iod_cat.d = [data_iod_cat.d; data_iod{i}.d];
        data_iod_cat.class = [data_iod_cat.class; data_cat{i}.class_IOD];
    end
end


%% Reshuffle the data, TODO: fix this hack

% data_cat.train = reshuffle_detection(data_cat.train);

if opts.IOD
    
    % Save the data as a struct
    save(opts.save_full, '-struct','data_iod_cat');

    % Save the copy
    try
        save(opts.save_copy, '-struct','data_iod_cat');
    catch
        disp('S drive not on path')
    end
    
else
    
    % Save the data as a struct
    save(opts.save_full, '-struct','data_cat');

    % Save the copy
    try
        save(opts.save_copy, '-struct','data_cat');
    catch
        disp('S drive not on path')
    end
end

% Compute those covariances
% opts.logger.info(mfilename, 'Entering EKF');
% run('EKF.m')

%% DIAGNOSTICS

if opts.PLOT_DIAGNOSTICS
    figure
    histogram(cell2mat(timers))
    title('Parallel Loop Times')
end

%% CLOSE AND CLEAN UP

% Close the pool if run in parallel
if opts.RUN_PARALLEL && opts.CLOSE_POOL
    opts.logger.info('Closing parallel pool');
    delete(poolobj)
end

% Diagnostic
disp('Successful data generation')
opts.logger.info(mfilename, 'Ending data generation');

t_main_end = toc(t_main)