classdef ContextManager < handle
    %CONTEXTMANAGER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        OM
    end
    
    methods
        
        function obj = ContextManager(opts_path)
            %CONTEXTMANAGER Construct an instance of this class
            %   INPUTS:
            %       opts.MATLAB.(x):
            %           WARNINGS - warnings on/off
            %   OUTPUTS:
            %       N/A
            
            % Get the option manager and load options
            obj.OM = OptsManager(opts_path);
            
            % Load the options
            opts = obj.OM.get_opts();
            
            % Generate the paths as specified in the options
            obj.gen_paths(opts.folders);
            
            obj.OM.parse_opts();
            
            % Who needs warnings
            if opts.MATLAB.flags.WARNINGS
                warning on all
            else
                warning off all
            end

            % Build the path
%             os_error_msg = 'Where''d you get this OS, the toilet store?';

            % Construct path
%             if ispc
%             %     cd(fileparts(which(localFilename)));
%                 cd(fileparts(which(mfilename)));
%                 top_path = pwd;
%             elseif isunix
%                 localFolder = '~/MEDICOM/MATLAB/';
%                 old_path = cd(localFolder);
%                 top_path = pwd;
%             else
%                 error(os_error_msg)
%             end
        end
        
        function opts = get_opts(obj)
        %GET_OPTS Getter for the options within the context manager
        %   INPUTS:
        %       N/A
        %   OUTPUTS:
        %       opts - options struct
        
            opts = obj.OM.get_opts();
            
        end
    end
    
    methods (Static)
        
        function gen_paths(folders)
            %GEN_PATHS Summary of this method goes here
            %   INPUTS:
            %       folders - 1-D cell array
            %   OUTPUTS:
            %       N/A
            
            % Iterate through each folder, recursively adding to the path
            for i = 1:length(folders)
                addpath(genpath(folders{i}));
            end
            
        end
    end
end

