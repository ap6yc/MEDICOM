.. MEDICOM documentation master file, created by
   sphinx-quickstart on Fri Jul 27 09:59:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:gitlab_url: Hosted on GitLab

MEDICOM
==================================

.. image:: assets/observatory512.png
    :height: 512
    :width: 512
    :scale: 50%

This site is the documentation page for the MEDICOM project, and stuff.

Contents:

.. toctree::
    :hidden:
    :caption: Home

    self

.. toctree::
    :maxdepth: 1
    :caption: Reference

    overview
    tutorial
    reference/index
    glossary
    help-support
    release-notes

.. toctree::
    :maxdepth: 1
    :caption: Legal

    legal/credits
    legal/license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
