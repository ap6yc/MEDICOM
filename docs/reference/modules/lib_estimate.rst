.. _lib_estimate:

=========================================
lib_estimate
=========================================

.. automodule:: pydemo.lib_estimate
    :members:
    :inherited-members:
    :private-members:
    :show-inheritance:
    :undoc-members:
