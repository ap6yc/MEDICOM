.. _lib_decorate:

=========================================
lib_decorate
=========================================

.. automodule:: pydemo.lib_decorate
    :members:
    :inherited-members:
    :private-members:
    :show-inheritance:
    :undoc-members:
