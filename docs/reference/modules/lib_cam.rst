.. _lib_cam:

=========================================
lib_cam
=========================================

.. automodule:: pydemo.lib_cam
    :members:
    :inherited-members:
    :private-members:
    :show-inheritance:
    :undoc-members:
