.. _lib_meta:

=========================================
lib_meta
=========================================

.. automodule:: pydemo.lib_meta
    :members:
    :inherited-members:
    :private-members:
    :show-inheritance:
    :undoc-members:
