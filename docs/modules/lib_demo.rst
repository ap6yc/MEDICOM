.. _lib_demo:

=========================================
lib_demo
=========================================

.. automodule:: pydemo.lib_demo
    :members:
    :inherited-members:
    :private-members:
    :show-inheritance:
    :undoc-members:
